<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Buku extends CI_Controller{
    public $id_menu = '213';

    public function __construct(){
        parent::__construct();
        cek_session();

        $data = check_role($this->id_menu, 2);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        $this->folder = $data->e_folder;
        $this->title = $data->e_menu;
        $this->icon = $data->icon;
        $this->i_company = $this->session->i_company;
        $this->load->model('m' . $this->folder, 'mymodel');
    }

    public function index(){
        add_css(
            array(
                'app-assets/vendors/css/tables/datatable/datatables.min.css',
				'app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css',
				'app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css',
				'app-assets/vendors/css/extensions/sweetalert.css',
            )
        );

        add_js(
			array(
				'app-assets/vendors/js/tables/datatable/datatables.min.js',
				'app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js',
				'app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js',
				'app-assets/vendors/js/extensions/sweetalert.min.js',
				'assets/js/' . $this->folder . '/index.js?v='.date('YmdHis'),
			)
		);
        $this->logger->write('Membuka Menu' . $this->title);
        $this->template->load('main', $this->folder . '/index');
    }

    public function serverside(){
        echo $this->mymodel->serverside();
    }

    public function add(){
        $data = check_role($this->id_menu, 1);
        if(!$data){
            redirect(base_url(), 'refresh');
        }

        add_css(
            array(
                'app-assets/css/plugins/forms/validation/form-validation.css',
				'app-assets/vendors/css/extensions/sweetalert2.min.css',
				'app-assets/vendors/css/animate/animate.css',
				'app-assets/vendors/css/forms/selects/select2.min.css',
				'app-assets/css/global.css',
            )
        );

        add_js(
            array(
                'app-assets/vendors/js/forms/validation/jqBootstrapValidation.js',
				'app-assets/vendors/js/extensions/sweetalert2.all.min.js',
				'app-assets/vendors/js/forms/select/select2.full.min.js',
				'assets/js/' . $this->folder . '/add.js?v='.date('YmdHis'),
            )
        );

        $this->logger->write('Membuka Form Tambah' . $this->title);
        $this->template->load('main', $this->folder . '/add');
    }

    public function save(){
        $data = check_role($this->id_menu, 1);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        $this->form_validation->set_rules('kode_buku', 'kode_buku', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('nama_buku', 'nama_buku', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('pengarang', 'pengarang', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('penerbit', 'penerbit', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('tahun_terbit', 'tahun_terbit', 'trim|required|min_length[0]');
        $kode_buku = $this->input->post('kode_buku', TRUE);
        $nama_buku = ucwords(strtolower($this->input->post('nama_buku', TRUE)));
        $pengarang = ucwords(strtolower($this->input->post('pengarang', TRUE)));
        $penerbit = ucwords(strtolower($this->input->post('penerbit', TRUE)));
        $tahun_terbit = $this->input->post('tahun_terbit', TRUE);

        // var_dump($kode_buku, $nama_buku, $pengarang, $penerbit, $tahun_terbit);
        // die;

        if($this->form_validation->run() == false){
            $data = array(
                'sukses' => false,
                'ada' => false,
            );
        } else{
            $cek = $this->mymodel->cek($kode_buku);
            if($cek->num_rows() > 0){
                $data = array(
                    'sukses' => false,
                    'ada' => true,
                );
            } else{
                $this->db->trans_begin();
                $this->mymodel->save($kode_buku, $nama_buku, $pengarang, $penerbit, $tahun_terbit);
                if($this->db->trans_status() == FALSE){
                    $this->db->trans_rollback();
                    $data = array(
                        'sukses' => false,
                        'ada' => false,
                    );
                } else {
                    $this->db->trans_commit();
                    $this->logger->write('Simpan' . $this->title . ' : ' . $kode_buku . ' : ' . $nama_buku . ' ; ' .
                    $pengarang . ' : ' . $penerbit . ' : ' . $tahun_terbit . ' : ' . $this->session->e_company_name);
                    $data = array(
                        'sukses' => true,
                        'ada' => false,
                    );
                }
            }
        }
        echo json_encode($data);
    }

    public function edit(){
        $data = check_role($this->id_menu, 3);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        add_css(
            array(
                'app-assets/css/plugins/forms/validation/form-validation.css',
				'app-assets/vendors/css/extensions/sweetalert2.min.css',
				'app-assets/vendors/css/animate/animate.css',
				'app-assets/vendors/css/forms/selects/select2.min.css',
				'app-assets/css/global.css',
            )
        );

        add_js(
            array(
                'app-assets/vendors/js/forms/validation/jqBootstrapValidation.js',
				'app-assets/vendors/js/extensions/sweetalert2.all.min.js',
				'app-assets/vendors/js/forms/select/select2.full.min.js',
				'assets/js/' . $this->folder . '/edit.js?v='.date('YmdHis'),
            )
        );

        $data = array(
            'data' => $this->mymodel->getdata(decrypt_url($this->uri->segment(3)))->row(),
        );
        $this->logger->write('Membuka Form Edit' . $this->title);
        $this->template->load('main', $this->folder . '/edit', $data);
    }

    public function view(){
        $data = check_role($this->id_menu, 2);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        add_css(
            array(
                'app-assets/css/plugins/forms/validation/form-validation.css',
				'app-assets/vendors/css/extensions/sweetalert2.min.css',
				'app-assets/vendors/css/animate/animate.css',
				'app-assets/vendors/css/forms/selects/select2.min.css',
				'app-assets/css/global.css',
            )
        );

        add_js(
            array(
                'app-assets/vendors/js/forms/validation/jqBootstrapValidation.js',
				'app-assets/vendors/js/extensions/sweetalert2.all.min.js',
				'app-assets/vendors/js/forms/select/select2.full.min.js',
				'assets/js/' . $this->folder . '/view.js?v='.date('YmdHis'),
            )
        );

        $data = array(
            'data' => $this->mymodel->getdata(decrypt_url($this->uri->segment(3)))->row(),
        );
        $this->logger->write('Membuka Form View' . $this->title);
        $this->template->load('main', $this->folder . '/view', $data);
    }

    public function delete(){
        $data = check_role($this->id_menu, 4);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        $this->form_validation->set_rules('id', 'id', 'trim|required|min_length[0]');
        $id = $this->input->post('id', TRUE);
        $i_buku_id = $this->db->get_where('buku', ['i_company' => $this->i_company, 'i_buku' => $id])->row()->i_buku_id;
        if($this->form_validation->run() == false){
            $data = array(
                'sukses' => false,
            );
        } else {
            $this->db->trans_begin();
            $this->mymodel->cancel($id);
            if($this->db->trans_status() == FALSE){
                $this->db->trans_rollback();
                $data = array(
                    'sukses' => false,
                );
            } else{
                $this->db->trans_commit();
                $this->logger->write('Batal' . $this->title . 'Id' . $id . ' : ' . $i_buku_id . ' : ' . $this->i_company);
                $data = array(
                    'sukses' => true,
                );
            }
        }
        echo json_encode($data);
    }

    public function update(){
        $data = check_role($this->id_menu, 3);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        $this->form_validation->set_rules('id', 'id', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('kode_buku', 'kode_buku', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('kode_buku_lama', 'kode_buku_lama', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('nama_buku', 'nama_buku', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('pengarang', 'pengarang', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('penerbit', 'penerbit', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('tahun_terbit', 'tahun_terbit', 'trim|required|min_length[0]');
        $id = $this->input->post('id', TRUE);
        $kode_buku = strtoupper($this->input->post('kode_buku', TRUE));
        $kode_buku_lama = strtoupper($this->input->post('kode_buku_lama', TRUE));
        $nama_buku = ucwords(strtolower($this->input->post('nama_buku', TRUE)));
        $pengarang = ucwords(strtolower($this->input->post('pengarang', TRUE)));
        $penerbit = ucwords(strtolower($this->input->post('penerbit', TRUE)));
        $tahun_terbit = $this->input->post('tahun_terbit', TRUE);

        if($this->form_validation->run() == false){
            $data = array(
                'sukses' => false,
                'ada' => false,
            );
        } else{
            $cek = $this->mymodel->cek_edit($kode_buku, $kode_buku_lama);
            if($cek->num_rows()>0){
                $data = array(
                    'sukses' => false,
                    'ada' => true,
                );
            } else{
                $this->db->trans_begin();
                $this->mymodel->update($id, $kode_buku, $nama_buku, $pengarang, $penerbit, $tahun_terbit);
                if($this->db->trans_status() == FALSE){
                    $this->db->trans_rollback();
                    $data = array(
                        'sukses' => false,
                        'ada' => false,
                    );
                } else{
                    $this->db->trans_commit();
                    $this->logger->write('Edit' . $this->title . ' : ' . $kode_buku . ' : ' . $nama_buku . ' : ' . $pengarang . ' : ' .
                    $penerbit . ' : ' . $tahun_terbit . ' : ' . $this->session->e_company_name);
                    $data = array(
                        'sukses' => true,
                        'ada' => false,
                    );
                }
            }
        }
        echo json_encode($data);
    }

    public function changestatus(){
        $data = check_role($this->id_menu, 3);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        $this->form_validation->set_rules('id', 'id', 'trim|required|min_length[0]');
        $id = $this->input->post('id', TRUE);
        $i__id = $this->db->get_where('buku', ['i_company' => $this->i_company, 'i_buku' => $id])->row()->e_buku_name;
        if($this->form_validation->run() == false){
            $data = array(
                'sukses' => false,
            );
        }else{
            $this->db->trans_begin();
            $this->mymodel->changestatus($id);
            if($this->db->trans_status() == FALSE){
                $this->db->trans_rollback();
                $data = array(
                    'sukses' => false,
                );
            }else{
                $this->db->trans_commit();
                $this->logger->write('Update Status' . $this->title . ' : ' . $id . ' : ' . $i__id . ' : ' . $this->session->e_buku_name);
                $data = array(
                    'sukses' => true,
                );
            }
        }
        echo json_encode($data);
    }
}