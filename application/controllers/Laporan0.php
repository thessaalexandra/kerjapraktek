<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Laporan extends CI_Controller{
    public $id_menu = '1772';

    public function __construct(){
        parent::__construct();
        cek_session();

        $data = check_role($this->id_menu, 2);
        if(!$data){
            redirect(base_url(), 'refresh');
        }

        $this->folder       = $data->e_folder;
        $this->title        = $data->e_menu;
        $this->icon         = $data->icon;
        $this->i_company    = $this->session->i_company;
        $this->i_user       = $this->session->i_user;

        $this->load->model('m' . $this->folder, 'mymodel');
    }

    public function index(){
        add_css(
            array(
                'app-assets/vendors/css/tables/datatable/datatables.min.css',
                'app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css',
                'app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css',
                'app-assets/vendors/css/extensions/sweetalert2.min.css',
                'app-assets/vendors/css/forms/selects/select2.min.css',
                'app-assets/vendors/css/animate/animate.css',
                'app-assets/vendors/css/pickers/pickadate/pickadate.css',
            )
        );

        add_js(
            array(
                'app-assets/vendors/js/tables/datatable/datatables.min.js',
                'app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js',
                'app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js',
                'app-assets/vendors/js/extensions/sweetalert2.all.min.js',
                'app-assets/vendors/js/forms/select/select2.full.min.js',
                'app-assets/vendors/js/pickers/pickadate/picker.js',
                'app-assets/vendors/js/pickers/pickadate/picker.date.js',
                'assets/js/' . $this->folder . '/index.js',
            )
        );

        $dfrom = $this->input->post('dfrom', TRUE);
        if($dfrom == ''){
            $dfrom == $this->uri->segment(3);
            if($dfrom == ''){
                $dfrom = '01-' . date('m-Y');
            }
        }

        $dto = $this->input->post('dto', TRUE);
        if($dto == ''){
            $dto = $this->uri->segment(4);
            if($dto == ''){
                $dto = date ('d-m-Y');
            }
        }

        $i_name = $this->input->post('i_name', TRUE);
        var_dump($i_name);
        die;
        // if($i_name == ''){
        //     $i_name = $this->uri->segment(5);
        //     if($i_name == ''){
        //         $i_name == 'ALL';
        //     }
        // }

        if(strlen($dfrom) != 10){
            $dfrom = decrypt_url($dfrom);
        }

        if(strlen($dto) != 20){
            $dto = decrypt_url($dto);
        }

        if(strlen($i_name) > 10){
            $i_name = decrypt_url($i_name);
        }
        // var_dump($i_name);
        // die;

        if ($i_name != 'ALL'){
            $e_name = $this->db->get_where('tesa', ['f_name_active' => true, 'i_company' => $this->i_company, 'i_name' => $i_name])->row()->e_name;
        } else{
            $e_name = 'Pilih Nama';
        }

        $data = array(
            'dfrom'     => date('d-m-Y', strtotime($dfrom)),
            'dto'       => date('d-m-Y', strtotime($dto)),
            'i_name'    => $i_name,
            'e_name'    => ucwords(strtolower($e_name)),
        );

        $this->logger->write('Membuka Menu' . $this->folder);
        $this->template->load('main', $this->folder . '/index');
    }

    public function serverside(){
        echo $this->mymodel->serverside();
    }

    /* Get Data Mahasiswa */
    public function get_mahasiswa(){
        $filter = [];
        $filter[] = array(
            'id'    => 'ALL',
            'text'  => 'Mahasiswa',
        );
        $data = $this->mymodel->get_mahasiswa(str_replace("'", "", $this->input->get('q')));
        foreach($data->result() as $row){
            $filter[] = array(
                'id'    => $row->i_name,
                'text'  => $row->i_name_id . ' - ' . $row->e_name,
            );
        }
        echo json_encode($filter);
    }

    public function view(){
        $data = check_role($this->id_menu, 2);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        add_js(
            array(
                'assets/js/' . $this->folder . '/index.js',
            )
        );

        $i_pinjam       = decrypt_url($this->uri->segment(3));
        $i_company      = decrypt_url($this->uri->segment(4));
        $dfrom          = decrypt_url($this->uri->segment(5));
        $dto            = decrypt_url($this->uri->segment(6));
        $i_name    = decrypt_url($this->uri->segment(7));

        if($i_name != 'ALL'){
            $e_name = $this->db->get_where('tesa', ['f_name_active' => true, 'i_company' => $this->i_company, 'i_name' => $i_name])->row()->e_name;
        } else{
            $e_name = 'Semua';
        }
        $i_pinjam_id = $this->db->get_where('peminjaman', ['i_pinjam' => $i_pinjam])->row()->i_pinjam_id;
        $data = array(
            'detail'        => $this->mymodel->get_data_detail($i_pinjam),
            'dfrom'         => $dfrom,
            'dto'           => $dto,
            'i_name'        => $i_name,
            'e_name'        => ucwords(strtolower($e_name)),
            'i_pinjam_id'   => $i_pinjam_id,
        );
        $this->logger->write('Membuka Form View' . $this->title);
        $this->template->load('main', $this->folder . '/view', $data);
    }

    public function export(){
        $dfrom          = $this->input->post('dfrom', TRUE);
        $dto            = $this->input->post('dto', TRUE);
        $i_name    = $this->input->post('i_name', TRUE);
        if($i_name != 'ALL'){
            $e_name = $this->db->get_where('tesa', ['f_name_active' => true, 'i_company' => $this->i_company, 'i_name' => $i_name])->row()->e_name;
        } else{
            $e_name = 'Semua Nama';
        }

        $query          = $this->mymodel->get_data($dfrom, $dto, $i_name);

        $spreadsheet    = new Spreadsheet;
        $sharedStyle1   = new Style();
        $sharedStyle2   = new Style();
        $sharedStyle3   = new Style();

        $spreadsheet->getActiveSheet()->getStyle('B2')->getAlignment()->applyFromArray(
            [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 'textRotation' => 0, 'wrapText' => TRUE
            ]
        );

        $sharedStyle1->applyFromArray(
            [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['rgb' => 'DFF1D0'],
                ],
                'font' => [
                    'name'  => 'Arial',
                    'bold'  => true,
                    'italic' => false,
                    'size'  => 10
                ],
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'top'    => ['borderStyle' => Border::BORDER_THIN],
                    'bottom' => ['borderStyle' => Border::BORDER_THIN],
                    'left'   => ['borderStyle' => Border::BORDER_THIN],
                    'right'  => ['borderStyle' => Border::BORDER_THIN]
                ],
            ]
        );

        $sharedStyle2->applyFromArray(
            [
                'font' => [
                    'name'  => 'Arial',
                    'bold'  => false,
                    'italic' => false,
                    'size'  => 10
                ],
                'borders' => [
                    'top'    => ['borderStyle' => Border::BORDER_THIN],
                    'bottom' => ['borderStyle' => Border::BORDER_THIN],
                    'left'   => ['borderStyle' => Border::BORDER_THIN],
                    'right'  => ['borderStyle' => Border::BORDER_THIN]
                ],
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                ],
            ]
        );

        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Calibri')
            ->setSize(9);

        $spreadsheet->getActiveSheet()->mergeCells("A1:I1");
        $spreadsheet->getActiveSheet()->mergeCells("A2:I2");
        $spreadsheet->getActiveSheet()->mergeCells("A3:I3");
        $spreadsheet->getActiveSheet()->mergeCells("A4:I4");
        $h = 6;
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', $this->title)
            ->setCellValue('A2', ucwords(strtolower($e_name)))
            ->setCellValue('A3', "Periode : $dfrom s/d $dto")
            ->setCellValue('A' . $h, $this->lang->line('No'))
            ->setCellValue('B' . $h, $this->lang->line('ID'))
            ->setCellValue('C' . $h, $this->lang->line('Nama Mahasiswa'))
            ->setCellValue('D' . $h, $this->lang->line('Jurusan'))
            ->setCellValue('E' . $h, $this->lang->line('Prodi'))
            ->setCellValue('F' . $h, $this->lang->line('ID Pinjam'))
            ->setCellValue('G' . $h, $this->lang->line('Tanggal Pinjam'))
            ->setCellValue('H' . $h, $this->lang->line('Total Pinjam'));
        
        $spreadsheet->getActiveSheet()->duplicateStyle($sharedStyle3, 'A1:I4');
        $spreadsheet->getActiveSheet()->duplicateStyle($sharedStyle1, 'A' . $h . ':I' . $h);

        if ($query->num_rows()>0){
            $i = 7;
            $x = 7;
            $nomor = 1;
            foreach ($query->result() as $row) {
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $nomor)
                    ->setCellValue('B' . $i, $row->i_pinjam_id)
                    ->setCellValue('C' . $i, $row->e_name)
                    ->setCellValue('D' . $i, $row->e_jurusan)
                    ->setCellValue('E' . $i, $row->e_prodi)
                    ->setCellValue('F' . $i, $row->i_pinjam_id)
                    ->setCellValue('G' . $i, $row->d_pinjam)
                    ->setCellValue('H' . $i, $row->v_nilai_bersih);
                $spreadsheet->getActiveSheet()->duplicateStyle($sharedStyle2, 'A' . $i . ':I' . $i);
                $i++;
                $nomor++;
            }
            $spreadsheet->getActiveSheet()
                ->getStyle('C' . $x . ':C' . $i)
                ->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
            $spreadsheet->getActiveSheet()->mergeCells('A' . $i . ':G' . $i);
            $spreadsheet->getActiveSheet()->duplicateStyle($sharedStyle2, 'A' . $i . ':I' . $i);
            $spreadsheet->getActiveSheet()->setCellValue('A' . $i, 'TOTAL');
            $spreadsheet->getActiveSheet()->setCellValue('H' . $i, "=SUM(H" . $x . ":H" . ($i - 1) . ")");
            $spreadsheet->getActiveSheet()
                ->getStyle('H' . $x . ':H' . $i)
                ->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_CURRENCY_IDR);   

        }

        $writer = new Xls($spreadsheet);
        $nama_file = $this->title . " - " . $e_company_name . "_" . $dfrom . "_sd_" . $dto . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $nama_file . '');
        header('Cache-Control: max-age=0');
        ob_start();
        $writer->save('php://output');
        $exceldata = ob_get_contents();
        ob_end_clean();
        $response =  array(
            'file'      => "data:application/vnd.ms-excel;base64," . base64_encode($exceldata),
            'nama_file' => $nama_file,
        );
        die(json_encode($response));

    }

}