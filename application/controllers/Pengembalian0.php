<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengembalian extends CI_Controller{
    public $id_menu = '2002';

    public function __construct(){
        parent::__construct();
        cek_session();

        $data = check_role($this->id_menu, 2);
        if(!$data){
            redirect(base_url(), 'refresh');
        }

        $this->folder       = $data->e_folder;
        $this->title        = $data->e_menu;
        $this->icon         = $data->icon;
        $this->i_company    = $this->session->i_company;
        $this->i_user       = $this->session->i_user;

        $this->load->model('m' . $this->folder, 'mymodel');
    }

    public function index(){
		add_css(
			array(
				'app-assets/vendors/css/tables/datatable/datatables.min.css',
				'app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css',
				'app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css',
				'app-assets/vendors/css/extensions/sweetalert2.min.css',
				'app-assets/vendors/css/animate/animate.css',
				'app-assets/vendors/css/pickers/pickadate/pickadate.css',
				'app-assets/vendors/css/forms/selects/select2.min.css',
			)
		);

		add_js(
			array(
				'app-assets/vendors/js/tables/datatable/datatables.min.js',
				'app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js',
				'app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js',
				'app-assets/vendors/js/extensions/sweetalert2.all.min.js',
				'app-assets/vendors/js/pickers/pickadate/picker.js',
				'app-assets/vendors/js/pickers/pickadate/picker.date.js',
				'assets/js/' . $this->folder . '/index.js?v='.date('YmdHis'),
				'app-assets/vendors/js/forms/select/select2.full.min.js',
            )
        );

        $dfrom = $this->input->post('dfrom', TRUE);
        if($dfrom == ''){
            $dfrom = $this->uri->segment(3);
            if($dfrom == ''){
                $dfrom = '01-' . date('m-Y');
            }
        }
        $dto = $this->input->post('dto', TRUE);
        if($dto == ''){
            $dto = $this->uri->segment(4);
            if($dto == ''){
                $dto = date('d-m-Y');
            }
        }

        $i_buku = $this->input->post('i_buku', TRUE);
        if($i_buku == ''){
            $i_buku = $this->uri->segment(5);
            if($i_buku == ''){
                $i_buku = '0';
            }
        }
        if(strlen($dfrom) != 10){
            $dfrom = decrypt_url($dfrom);
        }
        if(strlen($dto) != 10){
            $dto = decrypt_url($dto);
        }
        if(strlen($i_buku) > 10){
            $i_buku = decrypt_url($i_buku);
        }

        if($i_buku != '0'){
            $e_buku_name = $this->db->get_where('buku', ['f_buku_active' => true, 'i_company' => $this->i_company, 'i_buku' => $i_buku])->row()->e_buku_name;
        }else{
            $e_buku_name = 'PILIH BUKU';
        }

        $data = array(
            'dfrom' => date('d-m-Y', strtotime($dfrom)),
            'dto' => date('d-m-Y', strtotime($dto)),
            'i_buku' => $i_buku,
            'e_buku_name' => ucwords(strtolower($e_buku_name)),
        );
        $this->logger->write('Membuka Menu' . $this->title);
        $this->template->load('main', $this->folder . '/index', $data); 
    }

    public function serverside(){
        echo $this->mymodel->serverside();
    }

    public function get_buku(){
        $filter = [];
		$cari = str_replace("'", "", $this->input->get('q'));
		$imahasiswa = $this->input->get('i_name');
		if ($imahasiswa != '') {
			$data = $this->mymodel->get_buku($cari, $imahasiswa);
			foreach ($data->result() as $row) {
				$filter[] = array(
					'id'   => $row->i_kembali_item,
					'text' => $row->i_buku_id . ' - ' . $row->e_buku_name . ' - ( ' . $row->i_kembali_id . ' )',
				);
			}
		} else {
			$filter[] = array(
				'id'   => null,
				'text' => $this->lang->line('Pilih') . ' ' . $this->lang->line('Buku'),
			);
		}
		echo json_encode($filter);
    }

    public function add(){
        $data = check_role($this->id_menu, 1);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        add_css(
            array(
				'app-assets/css/plugins/forms/validation/form-validation.css',
				'app-assets/vendors/css/extensions/sweetalert2.min.css',
				'app-assets/vendors/css/animate/animate.css',
				'app-assets/vendors/css/forms/selects/select2.min.css',
				'app-assets/css/global.css',
			)
		);

		add_js(
			array(
				'app-assets/vendors/js/forms/validation/jqBootstrapValidation.js',
				'app-assets/vendors/js/extensions/sweetalert2.all.min.js',
				'app-assets/vendors/js/forms/select/select2.full.min.js',
				'assets/js/' . $this->folder . '/add.js?v='.date('YmdHis'),
			)
		);

        $dfrom = decrypt_url($this->uri->segment(3));
        $dto =  decrypt_url($this->uri->segment(4));
        $hbuku = decrypt_url($this->uri->segment(5));
        $data = array(
            'dfrom' => $dfrom,
            'dto' => $dto,
            'hbuku' => $hbuku,
        );
        $this->logger->write('Membuka Form Tambah' . $this->title);
        $this->template->load('main', $this->folder . '/add', $data);
    }

    public function number(){
        $tanggal = $this->input->post('tanggal', TRUE);
        if($tanggal != ''){
            $number = $this->mymodel->running_number(date('ym', strtotime($tanggal)), date('Y'), strtotime($tanggal));
        }else{
            $number = $this->mymodel->running_number(date('ym'), date('Y'));
        }
        echo json_encode($number);
    }

    public function get_mahasiswa(){
        $filter = [];
        $data = $this->mymodel->get_mahasiswa(str_replace("'", "", $this->input->get('q')));
        foreach($data->result() as $row){
            $filter[] = array(
                'id' => $row->i_name,
                'text' => $row->i_name_id . " - " . $row->e_name,
            );
        }
        echo json_encode($filter);
    }

    public function get_mahasiswa_detail(){
        header("Content-Type: application/json", true);
        $imahasiswa = $this->input->post('i_name', TRUE);
        $query = array(
            'header' => $this->mymodel->get_mahasiswa_detail($imahasiswa)->result_array()
        );
        echo json_encode($query);
    }


    // public function get_nama_buku(){
    //     $filter = [];
    //     $cari = str_replace("'", "", $this->input->get('q'));
    //     $i_name = $this->input->get('i_name');
    //     if($i_name != ''){
    //         $data = $this->mymodel->get_buku($cari, $imahasiswa);
    //         foreach ($data->result() as $row){
    //             $filter[] = array(
    //                 'id' => null,
    //                 'text' => $row->i_buku_id . ' - ( ' . $row->e_buku_name . ' ) ',
    //             );
    //         }
    //     } else{
    //         $filter[] = array(
    //             'id' => null,
    //             'text' => $this->lang->line('Pilih') . ' ' . $this->lang->line('Buku'),
    //         );
    //     }
    //     echo json_encode($filter);
    // }

    public function get_detail_buku(){
        $filter = [];
		$imahasiswa = $this->input->get('i_name');
		if ($imahasiswa != '') {
			$data = $this->mymodel->get_detail_buku(str_replace("'", "", $this->input->get('q')), $imahasiswa);
			foreach ($data->result() as $row) {
				$filter[] = array(
					'id'   => $row->i_buku,
					'text' => $row->i_buku_id . ' - ' . $row->e_buku_name,
				);
			}
		} else {
			$filter[] = array(
				'id'   => null,
				'text' => $this->lang->line('Pilih') . ' ' . $this->lang->line('Buku'),
			);
		}
		echo json_encode($filter);
    }

    // public function get_penerbit_buku(){
    //     $e_penerbit = $this->input->post('e_penerbit', TRUE);
    //     $i_name = $this->input->post('i_name', TRUE);
    //     $query = array(
    //         'detail' => $this->mymodel->get_penerbit_buku($e_penerbit, $i_name)->result_array()
    //     );
    //     echo json_encode($query);
    // }

    public function save(){
        $data = check_role($this->id_menu, 1);
        if(!$data){
            redirect(base_url(), 'refresh');
        }

		$this->form_validation->set_rules('i_name', 'i_name', 'trim|required|min_length[0]');
        // $this->form_validation->set_rules('i_buku', 'i_buku', 'trim|required|min_length[0]');
		$this->form_validation->set_rules('i_document', 'i_document', 'trim|required|min_length[0]');
		$this->form_validation->set_rules('d_document', 'd_document', 'trim|required|min_length[0]');
		$this->form_validation->set_rules('jml', 'jml', 'trim|required|min_length[0]');

        if($this->form_validation->run() == false){
            $data = array(
                'sukses' => false,
                'ada' => false,
            );
        } else{
            $cek = $this->mymodel->cek_code();
            if($cek->num_rows()>0){
                $data = array(
                    'sukses' => false,
                    'ada' => true,
                );
            } else{
                $this->db->trans_begin();
                $this->mymodel->save();
                if($this->db->trans_status() == FALSE){
                    $this->db->trans_rollback();
                    $data = array(
                        'sukses' => false,
                        'ada' => false,
                    );
                } else {
					$this->db->trans_commit();
					$this->logger->write('Simpan ' . $this->title . ' : ' . $this->input->post('i_document') . ' : ' . $this->session->e_company_name);
					$data = array(
						'sukses' => true,
						'ada'	 => false,
					);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function edit(){
        $data = check_role($this->id_menu, 3);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        add_css(
            array(
                'app-assets/css/plugins/forms/validation/form-validation.css',
				// 'app-assets/vendors/css/extensions/sweetalert.css',
				'app-assets/vendors/css/extensions/sweetalert2.min.css',
				'app-assets/vendors/css/forms/selects/select2.min.css',
				'app-assets/vendors/css/pickers/pickadate/pickadate.css',
				'app-assets/css/global.css',
            )
        );

        add_js(
            array(
                'app-assets/vendors/js/forms/validation/jqBootstrapValidation.js',
				// 'app-assets/vendors/js/extensions/sweetalert.min.js',
				'app-assets/vendors/js/extensions/sweetalert2_new.min.js',
				'app-assets/vendors/js/forms/select/select2.full.min.js',
				'app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js',
				'app-assets/vendors/js/pickers/pickadate/picker.js',
				'app-assets/vendors/js/pickers/pickadate/picker.date.js',
				'assets/js/' . $this->folder . '/edit.js?v=319177',
            )
        );

        $id = decrypt_url($this->uri->segment(3));
        $dfrom = decrypt_url($this->uri->segment(4));
        $dto = decrypt_url($this->uri->segment(5));
        $hbuku = decrypt_url($this->uri->segment(6));
        $data = array(
            'data' => $this->mymodel->get_data($id)->row(),
            'detail' => $this->mymodel->get_data_detail($id),
            'dfrom' => $dfrom,
            'dto' => $dto,
            'hbuku' => $hbuku,
        );
        $this->logger->write('Membuka Form Edit' . $this->title);
        $this->template->load('main', $this->folder . '/edit', $data); 
    }

    public function update(){
        $data = check_role($this->id_menu, 3);
        if(!$data){
            redirect(base_url(), 'refresh');
        }

        $this->form_validation->set_rules('id', 'id', 'trim|required|min_length[0]');
		// $this->form_validation->set_rules('i_buku', 'i_buku', 'trim|required|min_length[0]');
		// $this->form_validation->set_rules('i_name', 'i_name', 'trim|required|min_length[0]');
		$this->form_validation->set_rules('i_document', 'i_document', 'trim|required|min_length[0]');
		$this->form_validation->set_rules('i_document_old', 'i_document_old', 'trim|required|min_length[0]');
		$this->form_validation->set_rules('d_document', 'd_document', 'trim|required|min_length[0]');
		$this->form_validation->set_rules('jml', 'jml', 'trim|required|min_length[0]');

        if($this->form_validation->run() == false){
            $data = array(
                'sukses' => false,
                'ada' => false,
            );
        } else{
            $cek = $this->mymodel->cek_edit();
            if($cek->num_rows() > 0){
                $data = array(
                    'sukses' => false,
                    'ada' => true,
                );
            } else {
                $this->db->trans_begin();
                $this->mymodel->update();
                if($this->db->trans_status() == FALSE){
                    $this->db->trans_rollback();
                    $data = array(
                        'sukses' => false,
                        'ada' => false,
                    );
                } else {
                    $this->db->trans_commit();
                    $this->logger->write('Simpan ' . $this->title . ' : ' . $this->input->post('i_document')  . ' : ' . $this->session->e_company_name);
                    $data = array(
                        'sukses' => true,
                        'ada' => false,
                    );
                }
            }
        }
        echo json_encode($data);
    }

    public function delete(){
        $data = check_role($this->id_menu, 4);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        $this->form_validation->set_rules('id', 'id', 'trim|required|min_length[0]');
        $id = $this->input->post('id', TRUE);
        $i_kembali_id = $this->db->get_where('kembali', ['i_company' => $this->i_company, 'i_kembali' => $id])->row()->i_kembali_id;
        if($this->form_validation->run() == false){
            $data = array(
                'sukses' => false,
            );
        } else{
            $this->db->trans_begin();
            $this->mymodel->cancel($id);
            if($this->db->trans_status() == FALSE){
                $this->db->trans_rollback();
                $data = array(
                    'sukses' => false,
                );
            } else{
                $this->db->trans_commit();
                $this->logger->write('Batal ' . $this->title . ' Id : ' . $id . ' : ' . $i_kembali_id . ' : ' . $this->session->e_company_name);
				$data = array(
					'sukses' => true,
				);
            }
        }
        echo json_encode($data);
    }

    public function view(){
        $data = check_role($this->id_menu, 2);
        if(!$data){
            redirect(base_url(), 'refresh');
        }

        add_css(
			array(
				'app-assets/css/plugins/forms/validation/form-validation.css',
				// 'app-assets/vendors/css/extensions/sweetalert.css',
				'app-assets/vendors/css/extensions/sweetalert2.min.css',
				'app-assets/vendors/css/forms/selects/select2.min.css',
				'app-assets/vendors/css/pickers/pickadate/pickadate.css',
				'app-assets/css/global.css',
			)
		);

		add_js(
			array(
				'app-assets/vendors/js/forms/validation/jqBootstrapValidation.js',
				// 'app-assets/vendors/js/extensions/sweetalert.min.js',
				'app-assets/vendors/js/extensions/sweetalert2_new.min.js',
				'app-assets/vendors/js/forms/select/select2.full.min.js',
				'app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js',
				'app-assets/vendors/js/pickers/pickadate/picker.js',
				'app-assets/vendors/js/pickers/pickadate/picker.date.js',
				'assets/js/' . $this->folder . '/edit.js?v=319177',
			)
		);

        $id = decrypt_url($this->uri->segment(3));
        $dfrom = decrypt_url($this->uri->segment(4));
        $dto = decrypt_url($this->uri->segment(5));
        $hbuku = decrypt_url($this->uri->segment(6));
        $data = array(
            'data' => $this->mymodel->get_data($id)->row(),
            'detail' => $this->mymodel->get_data_detail($id),
            'dfrom' => $dfrom,
            'dto' => $dto,
            'hbuku' => $hbuku,
        );
        $this->logger->write('Membuka Form View' . $this->title);
        $this->template->load('main', $this->folder . '/view', $data);
    }

    public function print(){
        $data = check_role($this->id_menu, 5);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        add_css(
            array(
                'app-assets/css/bootstrap.min.css',
				'app-assets/css/bootstrap-extended.min.css',
				'app-assets/css/colors.min.css',
				'app-assets/css/components.min.css',
            )
        );
        add_js(
            array(
                'app-assets/vendors/js/vendors.min.js',
				'assets/js/' . $this->folder . '/print.js?v='.date('YmdHis'),
            )
        );
        $id = decrypt_url($this->uri->segment(3));
        $data = array(
            'data' => $this->mymodel->get_data(decrypt_url($this->uri->segment(3)))->row(),
            'detail' => $this->mymodel->get_data_detail(decrypt_url($this->uri->segment(3))),
        );
        $this->logger->write('Membuka Form Cetak' . $this->title);
        $this->load->view($this->folder . '/print', $data);
    }

    // public function update_print(){
    //     $this->form_validation->set_rules('id', 'id', 'trim|required|min_length[0]');
	// 	$id = $this->input->post('id', TRUE);
	// 	$i_kembali_id = $this->db->get_where('kembali', ['i_company' => $this->i_company, 'i_kembali'  => $id])->row()->i_kembali_id;
	// 	if ($this->form_validation->run() == false) {
	// 		$data = array(
	// 			'sukses' => false,
	// 		);
	// 	} else {
	// 		$this->db->trans_begin();
	// 		$this->mymodel->update_print($id);
	// 		if ($this->db->trans_status() === FALSE) {
	// 			$this->db->trans_rollback();
	// 			$data = array(
	// 				'sukses' => false,
	// 			);
	// 		} else {
	// 			$this->db->trans_commit();
	// 			$this->logger->write('Print ' . $this->title . ' Id : ' . $id . ' : ' . $i_kembali_id . ' : ' . $this->session->e_company_name);
	// 			$data = array(
	// 				'sukses' => true,
	// 			);
	// 		}
	// 	}
	// 	echo json_encode($data);
    // }
}