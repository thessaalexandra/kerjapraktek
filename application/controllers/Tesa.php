<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tesa extends CI_Controller{
    public $id_menu = '212';

    public function __construct(){
        parent::__construct();
        cek_session();

		$data = check_role($this->id_menu, 2);
		if (!$data) {
			redirect(base_url(), 'refresh');
		}
        $this->folder = $data->e_folder;
        $this->title = $data->e_menu;
        $this->icon = $data->icon;
        $this->i_company = $this->session->i_company;

        $this->load->model('m' . $this->folder, 'mymodel');
    }

    public function index(){
        add_css(
			array(
				'app-assets/vendors/css/tables/datatable/datatables.min.css',
				'app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css',
				'app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css',
				'app-assets/vendors/css/extensions/sweetalert.css',
			)
		);

		add_js(
			array(
				'app-assets/vendors/js/tables/datatable/datatables.min.js',
				'app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js',
				'app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js',
				'app-assets/vendors/js/extensions/sweetalert.min.js',
				'assets/js/' . $this->folder . '/index.js',
			)
		);
        $this->logger->write('Membuka Menu' . $this->title);
        $this->template->load('main', $this->folder . '/index');
    }

    public function serverside(){
        echo $this->mymodel->serverside();
    }

    public function add(){
        $data = check_role($this->id_menu, 1);
        if(!$data){
            redirect(base_url(), 'refresh');
        }

		add_css(
			array(
				'app-assets/css/plugins/forms/validation/form-validation.css',
				'app-assets/vendors/css/extensions/sweetalert2.min.css',
				'app-assets/vendors/css/animate/animate.css',
				'app-assets/vendors/css/forms/selects/select2.min.css',
				'app-assets/css/global.css',
			)
		);

		add_js(
			array(
				'app-assets/vendors/js/forms/validation/jqBootstrapValidation.js',
				'app-assets/vendors/js/extensions/sweetalert2.all.min.js',
				'app-assets/vendors/js/forms/select/select2.full.min.js',
				'assets/js/' . $this->folder . '/add.js?v='.date('YmdHis'),
			)
		);

        $this->logger->write('Membuka Form Tambah' . $this->title);
        $this->template->load('main', $this->folder . '/add');
    }

    public function save(){
        $data = check_role($this->id_menu, 1);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        $this->form_validation->set_rules('kode', 'kode', 'trim|required|min_length[0]');
		$this->form_validation->set_rules('nama', 'nama', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('jurusan', 'jurusan', 'trim|required|min_length[0]');   
		$this->form_validation->set_rules('prodi', 'prodi', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('umur', 'umur', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('jk', 'jk', 'trim|required|min_length[0]');
        $kode = strtoupper($this->input->post('kode', TRUE));
        $nama = ucwords(strtolower($this->input->post('nama', TRUE)));
        $alamat = ucwords(strtolower($this->input->post('alamat', TRUE)));
        $umur = $this->input->post('umur', TRUE);

        $jrsn = ($this->input->post('jurusan', TRUE));
        if ($jrsn == '01') {
            $jurusan = "S1 Informatika";
        } elseif($jrsn == '02') {
            $jurusan = "S1 Teknologi Informasi ";
        } elseif($jrsn == '03') {
            $jurusan = "S1 Rekayasa Perangkat Lunak ";
        } elseif($jrsn == '04') {
            $jurusan = "S1 MBTI ";
        } elseif($jrsn == '05') {
            $jurusan = "S1 Akuntasi ";
        } elseif($jrsn == '06') {
            $jurusan = "S1 Teknik Industri ";
        }else{
            $jurusan = "BODONG";
        }

        $prd = ($this->input->post('prodi', TRUE));
        if ($prd == '01'){
            $prodi = "Informatika";
        } elseif($prd == '02'){
            $prodi = "Ekonomi dan Bisnis";
        } elseif($prd == '03'){
            $prodi = "Rekayasa Industri";        
        }else{
            $prodi = "BODONG";
        }


        $jns = ($this->input->post('jk', TRUE));
        if ($jns == '01'){
            $jk = "Perempuan";
        } elseif($jns == '02'){
            $jk = "Laki-Laki";
        }else{
            $jk = "BODONG";
        }


        if($this->form_validation->run() == false){
            $data = array(
                'sukses' => false,
                'ada' => false,
            );
        } else {
            $cek = $this->mymodel->cek($kode);
            if($cek->num_rows() > 0){
                $data = array(
                    'sukses' => false,
                    'ada' => true,
                );
            } else{
                $this->db->trans_begin();
                $this->mymodel->save($kode, $nama, $jurusan, $prodi, $alamat, $umur, $jk);
                if($this->db->trans_status() == FALSE){
                    $this->db->trans_rollback();
                    $data = array(
                        'sukses' => false,
                        'ada' => false,
                    );
                } else{
                    $this->db->trans_commit();
					$this->logger->write('Simpan' . $this->title . ' : ' . $kode . ' : ' . $nama . ' : ' 
                    . $jurusan . ' : ' . $prodi . ' : ' . $alamat . ' : ' . $umur . ' : ' . $jk . ' : ' . $this->session->e_company_name);
					$data = array(
						'sukses' => true,
						'ada'	 => false,
					);
                }
            }
        }
        echo json_encode($data);
    }

    public function edit(){
        $data = check_role($this->id_menu, 3);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        add_css(
			array(
				'app-assets/css/plugins/forms/validation/form-validation.css',
				'app-assets/vendors/css/extensions/sweetalert2.min.css',
				'app-assets/vendors/css/animate/animate.css',
				'app-assets/vendors/css/forms/selects/select2.min.css',
				'app-assets/css/global.css',
			)
		);

		add_js(
			array(
				'app-assets/vendors/js/forms/validation/jqBootstrapValidation.js',
				'app-assets/vendors/js/extensions/sweetalert2.all.min.js',
				'app-assets/vendors/js/forms/select/select2.full.min.js',
				'assets/js/' . $this->folder . '/edit.js',
			)
		);

        $data = array(
            'data' => $this->mymodel->getdata(decrypt_url($this->uri->segment(3)))->row(),
        );
        $this->logger->write('Membuka Form Edit' . $this->title);
        $this->template->load('main', $this->folder . '/edit', $data);
    }

    public function view(){
        $data = check_role($this->id_menu, 2);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        add_css(
			array(
				'app-assets/css/plugins/forms/validation/form-validation.css',
				'app-assets/vendors/css/extensions/sweetalert2.min.css',
				'app-assets/vendors/css/animate/animate.css',
				'app-assets/vendors/css/forms/selects/select2.min.css',
				'app-assets/css/global.css',
			)
		);

		add_js(
			array(
				'app-assets/vendors/js/forms/validation/jqBootstrapValidation.js',
				'app-assets/vendors/js/extensions/sweetalert2.all.min.js',
				'app-assets/vendors/js/forms/select/select2.full.min.js',
				'assets/js/' . $this->folder . '/view.js',
			)
		);

        $data = array(
            'data' => $this->mymodel->getdata(decrypt_url($this->uri->segment(3)))->row(),
        );
        $this->logger->write('Membuka Form View' . $this->title);
        $this->template->load('main', $this->folder . '/view', $data);
    }

    public function delete(){
        $data = check_role($this->id_menu, 4);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        $this->form_validation->set_rules('id', 'id', 'trim|required|min_length[0]');
        $id = $this->input->post('id', TRUE);
        $i_name_id = $this->db->get_where('tesa', ['i_company' => $this->i_company, 'i_name' => $id])->row()->i_name_id;
        if($this->form_validation->run() == false){
            $data = array(
                'sukses' => false,
            );
        } else {
            $this->db->trans_begin();
            $this->mymodel->cancel($id);
            if ($this->db->trans_status() == FALSE){
                $this->db->trans_rollback();
                $data = array(
                    'sukses' => false,
                );
            } else{
                $this->db->trans_commit();
                $this->logger->write('Batal' . $this->title . 'Id: ' . $id . ':' . $i_name_id . ':' . $this->i_company);
                $data = array(
                    'sukses' => true,
                );
            }
        }
        echo json_encode($data);
    }

    public function update(){
        $data = check_role($this->id_menu, 3);
        if(!$data){
            redirect(base_url(), 'refresh');
        
        }
        $this->form_validation->set_rules('id', 'id', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('kode', 'kode', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('kodeOld', 'kodeOld', 'trim|required|min_length[0]');
		$this->form_validation->set_rules('nama', 'nama', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('jurusan', 'jurusan', 'trim|required|min_length[0]');   
		$this->form_validation->set_rules('prodi', 'prodi', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('umur', 'umur', 'trim|required|min_length[0]');
        $this->form_validation->set_rules('jk', 'jk', 'trim|required|min_length[0]');
        $id = $this->input->post('id', TRUE);
        $kode = strtoupper($this->input->post('kode', TRUE));
        $kodeOld = strtoupper($this->input->post('kodeOld', TRUE));
        $nama = ucwords(strtolower($this->input->post('nama', TRUE)));
        // $jurusan = ucwords(strtolower($this->input->post('jurusan', TRUE)));
        // $prodi = ucwords(strtolower($this->input->post('prodi', TRUE)));
        $alamat = ucwords(strtolower($this->input->post('alamat', TRUE)));
        $umur = $this->input->post('umur', TRUE);
        // $jk = ucwords(strtolower($this->input->post('jk', TRUE)));

        $jrsn = ($this->input->post('jurusan', TRUE));
        if ($jrsn == '01') {
            $jurusan = "S1 Informatika";
        } elseif($jrsn == '02') {
            $jurusan = "S1 Teknologi Informasi ";
        } elseif($jrsn == '03') {
            $jurusan = "S1 Rekayasa Perangkat Lunak ";
        } elseif($jrsn == '04') {
            $jurusan = "S1 MBTI ";
        } elseif($jrsn == '05') {
            $jurusan = "S1 Akuntasi ";
        } elseif($jrsn == '06') {
            $jurusan = "S1 Teknik Industri ";
        }else{
            $jurusan = $jrsn;
        }

        $prd = ($this->input->post('prodi', TRUE));
        if ($prd == '01'){
            $prodi = "Informatika";
        } elseif($prd == '02'){
            $prodi = "Ekonomi dan Bisnis";
        } elseif($prd == '03'){
            $prodi = "Rekayasa Industri";        
        }else{
            $prodi = $prd;
        }

        $jns = ($this->input->post('jk', TRUE));
        if ($jns == '01'){
            $jk = "Perempuan";
        } elseif($jns == '02'){
            $jk = "Laki-Laki";
        }else{
            $jk = $jns;
        }

        if($this->form_validation->run() == false){
            $data = array(
                'sukses' => false,
                'ada' => false,
            );
        }else{
            $cek = $this->mymodel->cek_edit($kode, $kodeOld);
            if($cek->num_rows() > 0){
                $data = array(
                    'sukses' => false,
                    'ada' => true,
                );
            } else{
                $this->db->trans_begin();
                $this->mymodel->update($id, $kode, $nama, $jurusan, $prodi, $alamat, $umur, $jk);
                if($this->db->trans_status() == FALSE){
                    $this->db->trans_rollback();
                    $data = array(
                        'sukses' => false,
                        'ada' => false,
                    );
                } else{
                    $this->db->trans_commit();
                    $this->logger->write('Edit' . $this->title . ' : ' . $kode . ' : ' . $nama . ' : ' . $jurusan . ' : ' . $prodi . ' : ' . $alamat . ' : ' . $umur . ' : ' . $jk . ' : ' . $this->session->e_company_name);
                    $data = array(
                        'sukses' => true,
                        'ada' => false,
                    );
                }
            }
        }
        echo json_encode($data);
    }

    public function changestatus(){
        $data = check_role($this->id_menu, 3);
        if(!$data){
            redirect(base_url(), 'refresh');
        }
        $this->form_validation->set_rules('id', 'id', 'trim|required|min_length[0]');
        $id = $this->input->post('id', TRUE);
        $i__id = $this->db->get_where('tesa', ['i_company' => $this->i_company, 'i_name' => $id])->row()->e_name;
        if($this->form_validation->run() == false){
            $data = array(
                'sukses' => false,
            );
        }else{
            $this->db->trans_begin();
            $this->mymodel->changestatus($id);
            if($this->db->trans_status() == FALSE){
                $this->db->trans_rollback();
                $data = array(
                    'sukses' => false,
                );
            }else{
                $this->db->trans_commit();
                $this->logger->write('Update Status ' . $this->title . ' Id : ' . $id . ' : ' . $i__id . ' : ' . $this->session->e_name);
                $data = array(
                    'sukses' => true,
                );
            }
        }
        echo json_encode($data);
    }
}