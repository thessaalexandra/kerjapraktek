<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\CodeigniterAdapter;

class Mbuku extends CI_Model{

    public function serverside(){
        $datatables = new Datatables(new CodeigniterAdapter);
        $datatables->query("SELECT
        i_buku,
        i_buku_id,
        e_buku_name,
        e_pengarang,
        e_penerbit,
        n_tahun_terbit,
        f_buku_active FROM buku WHERE i_company = '$this->i_company'
        ", FALSE);

        $datatables->edit('f_buku_active', function($data){
            $id = $data['f_buku_active'];
            if($data['f_buku_active'] == 't'){
                $status = $this->lang->line('Aktif');
                $color = 'teal';
            }else{
                $status = $this->lang->line('Batal');
                $color = 'red';
            }

            $data = "<span class='badge bg-" . $color . "badge-pill'>" . $status . "</span>";
            return $data; 
        });

        if(check_role($this->id_menu, 3)){
            $datatables->add('action', function($data){
                $id = trim($data['i_buku']);
                $f_buku_active = $data['f_buku_active'];
                $data = '';
                $data .= "<a href='" . base_url() . $this->folder . '/view/' . encrypt_url($id) . "' title='View Data'><i class='fa fa-eye fa-lg warning darken-4 mr-1'></i></a>";
                if(check_role($this->id_menu, 3) && $f_buku_active == 't'){
                    $data .= "<a href='" . base_url() . $this->folder . '/edit/' . encrypt_url($id) . "' title ='Edit Data'><i class='fa fa-pencil-square success darken-4 fa-lg mr-1'></i></a>";
                }
                if(check_role($this->id_menu, 4) && $f_buku_active == 't'){
                    $data .= "<a href='#' onclick='sweetdelete(\"" . $this->folder . "\",\"" . $id . "\");' title='Hapus Data'><i class='fa fa-trash fa-lg red darken-4'></i></a>";
                }
                return $data;
            });
        }
        return $datatables->generate();
    }

    public function changestatus($id){
        $this->db->select('f_buku_active');
        $this->db->from('buku');
        $this->db->where('i_buku', $id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $status = $query->row()->f_buku_active;
        }else{
            $status = 'f';
        }
        if($status == 'f'){
            $fstatus = 't';
        } else{
            $fstatus = 'f';
        }
        $table = array(
            'f_buku_active' => $status,
        );
        $this->db->where('i_buku', $id);
        $this->db->update('buku', $table);
    }

    public function cek($kode_buku){
        return $this->db->query("SELECT
        i_buku_id FROM buku WHERE i_buku_id = '$kode_buku' AND i_company = '$this->i_company'
        ", FALSE);
    }

    public function save($kode_buku, $nama_buku, $pengarang, $penerbit, $tahun_terbit){
        
        $table = array(
            'i_company' => $this->i_company,
            'i_buku_id' => $kode_buku,
            'e_buku_name' => $nama_buku,
            'e_pengarang' => $pengarang,
            'e_penerbit' => $penerbit,
            'n_tahun_terbit' => $tahun_terbit,
        );
        $this->db->insert('buku', $table);
    }

    public function getdata($id){
        return $this->db->query("SELECT
        * FROM buku WHERE i_buku = '$id'
        ", FALSE);
    }

    public function cek_edit($kode_buku, $kode_buku_lama){
        return $this->db->query("SELECT
        'i_buku_id' FROM buku WHERE
        trim(upper(i_buku_id)) <> trim(upper('$kode_buku_lama')) AND
        trim(upper(i_buku_id)) = trim(upper('$kode_buku')) AND
        i_company = '$this->i_company'
        ", FALSE);
    }

    public function update($id, $kode_buku, $nama_buku, $pengarang, $penerbit, $tahun_terbit){
        $table = array(
            'i_company' => $this->i_company,
            'i_buku_id' => $kode_buku,
            'e_buku_name' => $nama_buku,
            'e_pengarang' => $pengarang,
            'e_penerbit' => $penerbit,
            'n_tahun_terbit' => $tahun_terbit,
        );
        $this->db->where('i_buku', $id);
        $this->db->update('buku', $table);
    }

    public function cancel($id){
        $table = array(
            'f_buku_active' => 'f',
        );
        $this->db->where('i_buku', $id);
        $this->db->update('buku', $table);
    }
}