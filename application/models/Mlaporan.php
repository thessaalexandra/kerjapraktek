<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\CodeigniterAdapter;

class Mlaporan extends CI_Model
{

    /**** List Datatable ***/
    public function serverside()
    {
        $dfrom = $this->input->post('dfrom', TRUE);
        if ($dfrom == '') {
            $dfrom = $this->uri->segment(3);
        }

        $dto = $this->input->post('dto', TRUE);
        if ($dto == '') {
            $dto = $this->uri->segment(4);
        }


        $i_name = $this->input->post('i_name', TRUE);
        if($i_name == ''){
            $i_name = $this->uri->segment(5);
        }

        if($i_name != 'ALL'){
            $mahasiswa = " AND a.i_name = '$i_name' ";
        } else{
            $mahasiswa = "";
        }

        $dfrom  = date('Y-m-d', strtotime($dfrom));
        $dto    = date('Y-m-d', strtotime($dto));

        $datatables = new Datatables(new CodeigniterAdapter);
        $datatables->query("xSELECT
                a.i_company,
                a.i_pinjam,
                b.i_name_id,
                b.e_name as e_name,
                b.e_jurusan,
                b.e_prodi,
                a.i_pinjam_id,
                a.d_pinjam,
                a.v_nilai_bersih:: money as v_nilai_bersih,
                '$dfrom' AS dfrom,
                '$dto' AS dto,
                '$i_name' AS i_name
            FROM
                peminjaman a
            INNER JOIN tesa b on (b.i_name = a.i_name)
            WHERE
                a.d_pinjam BETWEEN '$dfrom' AND '$dto'
                AND a.f_pinjam_cancel = 'f'
                AND a.i_company = '$this->i_company'
                $mahasiswa
            order by 3
        ", FALSE);

        /** Cek Hak Akses, Apakah User Bisa Edit */
        $datatables->add('action', function ($data) {
            $i_company  = $data['i_company'];
            $dfrom      = $data['dfrom'];
            $dto        = $data['dto'];
            $i_name     = $data['i_name'];
            $i_pinjam   = $data['i_pinjam'];
            $data       = '';
            $data      .= "<a href='" . base_url() . $this->folder . '/view/' . encrypt_url($i_pinjam) . '/' . encrypt_url($i_company) . '/' . encrypt_url($dfrom) . '/' . encrypt_url($dto) . '/' . encrypt_url($i_name) . "' title='View Mutasi'><i class='fa fa-eye fa-lg warning darken-4 mr-1'></i></a>";
            return $data;
        });
        $datatables->hide('i_company');
        $datatables->hide('dfrom');
        $datatables->hide('dto');
        $datatables->hide('i_name');
        return $datatables->generate();
    }


    public function get_mahasiswa($cari){
        return $this->db->query("SELECT
                distinct
                a.i_name,
                a.i_name_id,
                a.e_name
            FROM
                tesa a
            INNER JOIN peminjaman b ON (b.i_name = a.i_name)
            WHERE
                (e_name ILIKE '%$cari%' OR i_name_id ILIKE '%$cari%')
                AND a.i_company = '$this->i_company'
                AND f_name_active = true
            ORDER BY
            3
        ", FALSE);
    }

    /**** List Mutasi ***/
    public function get_data_detail($i_pinjam){
        return $this->db->query("SELECT
                a.i_pinjam_item,
                a.i_pinjam,
                b.e_buku_name,
                b.e_penerbit,
                a.n_quantity,
                a.n_diskon,
                (a.n_harga * a.n_quantity) * (a.n_diskon / 100) as jml    
            FROM peminjaman_item a
            INNER JOIN
                buku b on (b.i_buku = a.i_buku)
            WHERE
                a.i_pinjam = '$i_pinjam'
            ORDER BY n_item_no DESC
        ", FALSE);
    }
    /** Get Data Untuk Export */
    public function get_data($dfrom, $dto, $i_name){
        $dfrom  = date('Y-m-d', strtotime($dfrom));
        $dto    = date('Y-m-d', strtotime($dto));
        if($i_name != 'ALL'){
            $mahasiswa = " AND a.i_name = '$i_name' ";
        } else{
            $mahasiswa = "";
        }
        return $this->db->query("SELECT
                a.i_company,
                a.i_pinjam,
                a.i_pinjam_id,
                b.i_name_id || ' - ' || b.e_name as e_name,
                b.e_jurusan,
                b.e_prodi,
                a.d_pinjam,
                a.v_nilai_bersih as v_nilai_bersih,
                '$dfrom' AS dfrom,
                '$dto' AS dto,
                '$i_name' AS i_name
            FROM
                peminjaman a
            INNER JOIN tesa b on (b.i_name = a.i_name)
            WHERE
                a.d_pinjam BETWEEN '$dfrom' AND '$dto'
                AND a.f_pinjam_cancel = 'f'
                AND a.i_company = '$this->i_company'
                $mahasiswa
            ORDER BY 3
        ", FALSE);
    }
}

/* End of file Mmaster.php */
