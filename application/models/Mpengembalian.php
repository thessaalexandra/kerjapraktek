<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\CodeigniterAdapter;

class Mpengembalian extends CI_Model{

    public function serverside(){
        $dfrom = $this->input->post('dfrom', TRUE);
        if($dfrom == ''){
            $dfrom = $this->uri->segment(3);
        }

        $dto = $this->input->post('dto', TRUE);
        if($dto == ''){
            $dto = $this->uri->segment(4);
        }

        $i_pinjam = $this->input->post('i_pinjam', TRUE);
        if($i_pinjam == ''){
            $i_pinjam = $this->uri->segment(5);
        }

        if($i_pinjam != '0'){
            $pinjam = "AND a.i_pinjam = '$i_pinjam'";
        }else{
            $pinjam = "";
        }

        $dfrom  = date('Y-m-d', strtotime($dfrom));
        $dto    = date('Y-m-d', strtotime($dto));

        $datatables = new Datatables(new CodeigniterAdapter);
        $datatables->query("SELECT
            a.i_company,
            a.i_kembali as id,
            a.i_kembali_id,
            c.i_name_id || ' ~ ' || c.e_name as e_namee,
            a.d_kembali,
            a.e_remark,
            a.f_kembali_cancel,
            
            '$dfrom' as dfrom,
            '$dto' as dto,
            '$i_pinjam' AS i_pinjam
        FROM
            kembali a
        INNER JOIN tesa c on
            (c.i_name = a.i_name)
        WHERE
            a.i_company = '$this->i_company'
            AND a.d_kembali BETWEEN '$dfrom' AND '$dto'
            
        ORDER BY
            1 DESC
        ", FALSE);

        $datatables->edit('f_kembali_cancel', function ($data){
            if($data['f_kembali_cancel'] == 't'){
                $status = $this->lang->line('Batal');
                $color = 'red';
            }else{
                $color = 'teal';
                $status = $this->lang->line('Aktif');
            }
            $data = "<span class='badge bg-" . $color . " badge-pill'>" . $status . "</span>";
            return $data;
        });

        $datatables->add('action', function($data){
            $id = trim($data['id']);
            $f_kembali_cancel = $data['f_kembali_cancel'];
            $dfrom = $data['dfrom'];
            $dto = $data['dto'];
            $i_pinjam = $data['i_pinjam'];
            $data = '';
            $data .= "<a href='" . base_url() . $this->folder . '/view/' . encrypt_url($id) . '/' . encrypt_url($dfrom) . '/' . encrypt_url($dto) . '/' . encrypt_url($i_pinjam) . "' title='View Data'><i class='fa fa-eye fa-lg warning darken-4 mr-1'></i></a>";
            if(check_role($this->id_menu, 3) && $f_kembali_cancel == 'f'){
                $data .= "<a href='" . base_url() . $this->folder . '/edit/' . encrypt_url($id) . '/' . encrypt_url($dfrom) . '/' . encrypt_url($dto) . '/' . encrypt_url($i_pinjam) . "' title ='Edit Data'><i class='fa fa-pencil-square success darken-4 fa-lg mr-1'></i></a>";
            }
            if(check_role($this->id_menu, 4) && $f_kembali_cancel == 'f'){
                $data      .= "<a href='#' onclick='sweetdeletetesa(\"" . $this->folder . "\",\"" . $id . "\",\"" . encrypt_url($dfrom)  . "\",\"" . encrypt_url($dto)  . '/' . encrypt_url($i_pinjam) . "\");' title='Hapus Data'><i class='fa fa-trash fa-lg red darken-4'></i></a>";
            } 
            if(check_role($this->id_menu, 5) && $f_kembali_cancel == 'f') {
                $data      .= "<a href='#' onclick='openLink(\"" . $this->folder . "\",\"" . encrypt_url($id) . "\"); return false;' title='Print Data'><i class='fa fa-print fa-lg blue darken-4 mr-1'></i></a>";
            }
            return $data;
        });

        $datatables->hide('dfrom');
        $datatables->hide('dto');
        $datatables->hide('i_pinjam');
        $datatables->hide('i_company');
        return $datatables->generate();
    }

    public function get_mahasiswa($cari){
        return $this->db->query("SELECT
        DISTINCT
            a.i_name,
            a.i_name_id,
            initcap(e_name) AS e_name
        FROM
            tesa a
        WHERE
            (e_name ILIKE '%$cari%' OR i_name_id ILIKE '%$cari%')
            AND i_company = '$this->i_company'
            AND f_name_active = 'true'
        ORDER BY 3 ASC
        ", FALSE);
    }

    public function get_mahasiswa_detail($imahasiswa){
        return $this->db->query("SELECT
            a.i_name,
            a.i_name_id,
            a.e_jurusan,
            a.e_prodi,
            a.e_alamat,
            a.e_umur,
            a.e_jk
        FROM tesa a
        WHERE i_name = $imahasiswa
        ", FALSE);
    }

    public function get_pinjam($cari, $i_name){
        return $this->db->query("SELECT
        DISTINCT
            a.i_pinjam_item,
            a.i_buku,
            b.i_pinjam_id,
            c.i_buku_id,
            c.e_buku_name
        FROM
            peminjaman_item a
        INNER JOIN peminjaman b ON
            (b.i_pinjam = a.i_pinjam)
        INNER JOIN buku c on
            (c.i_buku = a.i_buku)
        WHERE
            b.i_company = '$this->i_company'
            AND b.f_pinjam_cancel = 'f'
            AND b.i_name = '$i_name'
            AND (c.e_buku_name ILIKE '%$cari%'
            OR c.i_buku_id ILIKE '%$cari%')
        ORDER BY 3 DESC
        ", FALSE);
    }

    public function get_detail_pinjam($i_pinjam_item, $imahasiswa){
        
        return $this->db->query("SELECT
        a.i_pinjam_item,
        a.i_buku,
        a.n_quantity,
        a.n_diskon,
        a.n_harga,
        b.e_pengarang,
        b.e_penerbit,
        b.n_tahun_terbit,
        b.harga_buku,
        b.e_buku_name
    from
        peminjaman_item a
        inner join buku b on (b.i_buku = a.i_buku)
    where
        i_pinjam_item = '$i_pinjam_item'  
        ", FALSE);
    } 

    public function running_number($thbl, $tahun)
    {
        $cek = $this->db->query("SELECT 
                substring(i_kembali_id, 1, 3) AS kode 
            FROM kembali
            WHERE i_company = '$this->i_company'
            AND f_kembali_cancel = 'f'
            ORDER BY i_kembali_id DESC
            ");
        if ($cek->num_rows() > 0) {
            $kode = $cek->row()->kode;
        } else {
            $kode = 'KMB';
        }
        $query  = $this->db->query("SELECT
                max(substring(i_kembali_id, 10, 6)) AS max
            FROM
                kembali
            WHERE to_char (d_kembali, 'yyyy') >= '$tahun'
            AND i_company = '$this->i_company'
            AND f_kembali_cancel = 'f'
            AND substring(i_kembali_id, 1, 3) = '$kode'
            AND substring(i_kembali_id, 5, 2) = substring('$thbl',1,2)
        ", false);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $no = $row->max;
            }
            $number = $no + 1;
            settype($number, "string");
            $n = strlen($number);
            while ($n < 6) {
                $number = "0" . $number;
                $n = strlen($number);
            }
            $number = $kode . "-" . $thbl . "-" . $number;
            return $number;
        } else {
            $number = "00001";
            $nomer  = $kode . "-" . $thbl . "-" . $number;
            return $nomer;
        }
    }

    public function cek_code(){
        $i_document = str_replace("_", "", $this->input->post('i_document'));
        return $this->db->query("SELECT
                i_kembali_id
            FROM
                kembali
            WHERE
                upper(trim(i_kembali_id)) = upper(trim('$i_document'))
                AND i_company = '$this->i_company'
        ", FALSE);
    }

    public function save(){        
        $query = $this->db->query("SELECT max(i_kembali)+1 AS id FROM kembali", TRUE);
        if ($query->num_rows() > 0) {
            $id = $query->row()->id;
            if ($id == null) {
                $id = 1;
            } else {
                $id = $id;
            }
        } else {
            $id = 1;
        }

        $ym = date('ym', strtotime($this->input->post('d_document')));
        $Y = date('Y', strtotime($this->input->post('d_document')));
        $header = array(
            'i_company'         => $this->session->i_company,
            'i_name'            => $this->input->post('i_name'),
            'i_kembali'         => $id,
            'd_kembali'         => $this->input->post('d_document'),
            'd_kembali_entry'   => current_datetime(),
            'f_kembali_cancel'  => 'f',
            'e_remark'          => $this->input->post('e_remark'),
            'v_nilai_bersih'    => str_replace(",", "",$this->input->post('subtotal')),
            'i_kembali_id'      => strtoupper($this->input->post('i_document')),
        );
        $this->db->insert('kembali', $header);
        // if ($this->input->post('jml') > 0) {
            if (is_array($this->input->post('i_pinjam_item')) || is_object($this->input->post('i_pinjam_item'))) {
                $i = 0;
                foreach ($this->input->post('i_pinjam_item') as $i_pinjam_item) {
                    $item = array(
                        'i_kembali'         => $id,
                        'i_pinjam_item'     => $i_pinjam_item,
                        'i_buku'            => $this->input->post('i_buku')[$i],
                        'e_buku_name'       => $this->input->post('e_buku_name')[$i],
                        'e_remark_item'     => $this->input->post('e_remark_item')[$i],
                        'n_harga'           => $this->input->post('harga_buku')[$i],
                        'n_quantity'        => $this->input->post('n_quantity')[$i],
                        'n_diskon'          => $this->input->post('n_diskon')[$i],
                        'n_item_no'         => $i,
        
                    );
                    $this->db->insert('kembali_item', $item);
                    $i++;
                // }
            }
        } else {
            die;
        }
    }

    public function get_data($id){
        return $this->db->query("SELECT
                a.*,
                d.e_name,
                d.e_prodi,
                d.e_alamat,
                d.e_jurusan,
                d.e_jk,
                d.e_umur
            FROM 
                kembali a
            INNER JOIN tesa d ON
                (d.i_name = a.i_name)
            WHERE i_kembali = '$id'
        ", FALSE);
    }

    public function get_data_detail($id){
        return $this->db->query("SELECT
        a.*,
        c.i_buku_id,
        c.e_pengarang,
        c.e_penerbit,
        c.n_tahun_terbit,
        c.e_buku_name,
        c.harga_buku,
        (a.n_quantity * a.n_harga) - ((a.n_quantity * a.n_harga) * (n_diskon / 100)) AS v_total
        -- (a.n_quantity * a.n_harga) * (n_diskon / 100) AS v_total
        FROM
            kembali_item a
        INNER JOIN kembali b ON (b.i_kembali = a.i_kembali)
        INNER JOIN buku c ON (a.i_buku = c.i_buku)
        WHERE 
            a.i_kembali = '$id'
        ", FALSE);
    }

    public function cek_edit(){
        $i_document = str_replace("_", "", $this->input->post('i_document'));
        $i_document_old = str_replace("_", "", $this->input->post('i_document_old'));
        return $this->db->query("SELECT
            i_kembali_id
        FROM
            kembali
        WHERE
            trim(upper(i_kembali_id)) <> trim(upper('$i_document_old'))
            AND trim(upper(i_kembali_id)) = trim(upper('$i_document'))
            AND i_company = '$this->i_company'        
        ", FALSE); 
    }

    public function update(){
        $id = $this->input->post('id');
        $header = array(
            'i_company'         => $this->session->i_company,
            'i_name'            => $this->input->post('i_name'),
            'i_kembali'          => $id,
            'd_kembali'         => $this->input->post('d_document'),
            'd_kembali_update'  => current_datetime(),
            'f_kembali_cancel'  => 'f',
            'e_remark'          => $this->input->post('e_remark'),
            'v_nilai_bersih'    => str_replace(",", "",$this->input->post('subtotal')),
            'i_kembali_id'      => strtoupper($this->input->post('i_document')),
            // 'v_nilai_bersih'    => '0',
        );
        $this->db->where('i_kembali', $id);
        $this->db->update('kembali', $header);
        // $jml = $this->input->post('jml');
        if ($this->input->post('jml') > 0) {
            $i = 0;
            if (is_array($this->input->post('i_pinjam_item')) || is_object($this->input->post('i_pinjam_item'))) {
                $this->db->where('i_kembali', $id);
                $this->db->delete('kembali_item');
                foreach ($this->input->post('i_pinjam_item') as $i_pinjam_item) {
                    $item = array(
                        'i_kembali'         => $id,
                        'i_pinjam_item'     => $i_pinjam_item,
                        'i_buku'            => $this->input->post('i_buku')[$i],
                        'e_buku_name'       => $this->input->post('e_buku_name')[$i],
                        'e_remark_item'     => $this->input->post('e_remark_item')[$i],
                        'n_harga'           => $this->input->post('harga_buku')[$i],
                        'n_quantity'        => $this->input->post('n_quantity')[$i],
                        'n_diskon'          => $this->input->post('n_diskon')[$i],
                        'n_item_no'         => $i,
                    );
                    $this->db->insert('kembali_item', $item);
                    $i++;
                }
            }
        } else {
            die;
        }
    }

    public function cancel($id)
    {
        $table = array(
            'f_kembali_cancel' => 't',
        );
        $this->db->where('i_kembali', $id);
        $this->db->update('kembali', $table);
    }

}