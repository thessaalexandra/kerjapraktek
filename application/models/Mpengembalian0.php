<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\CodeigniterAdapter;

class Mpeminjaman extends CI_Model{
    
    public function serverside(){
        $dfrom = $this->input->post('dfrom', TRUE);
        if($dfrom == ''){
            $dfrom = $this->uri->segment(3);
        }

        $dto = $this->input->post('dto', TRUE);
        if($dto == ''){
            $dto = $this->uri->segment(4);
        }

        $i_buku = $this->input->post('i_buku', TRUE);
        if($i_buku == ''){
            $i_buku = $this->uri->segment(5);
        }

        if($i_buku != '0'){
            $buku = "AND a.i_buku = '$i_buku'";
        }else{
            $buku = "";
        }

        $dfrom = date('Y-m-d', strtotime($dfrom));
        $dto = date('Y-m-d', strtotime($dto));

        $datatables = new Datatables(new CodeigniterAdapter);
        $datatables->query("SELECT
            a.i_pinjam as id,
            a.i_company,
            a.i_pinjam_id,
            c.i_name_id || ' ~ ' || c.e_name as e_namee,
            a.d_pinjam,
            a.d_pengembalian,
            a.e_remark,
            a.f_pinjam_cancel,
            '$dfrom' AS dfrom,
            '$dto' AS dto,
            '$i_buku' AS i_buku
        FROM
            peminjaman a
        -- INNER JOIN buku b ON
        --     (b.i_buku = a.i_buku)
        INNER JOIN tesa c on
            (c.i_name = a.i_name)
        WHERE
        a.i_company = '$this->i_company'
            AND a.d_pinjam BETWEEN '$dfrom' AND '$dto'
            $buku
        ORDER BY
            1 DESC       
        ", FALSE);

        $datatables->edit('f_pinjam_cancel', function($data){
            if($data['f_pinjam_cancel'] == 't'){
                $status = $this->lang->line('Batal');
                $color = 'red';
            } else{
                $color = 'teal';
                $status = $this->lang->line('Aktif');
            }
            $data = "<span class='badge bg-" . $color . " badge-pill'>" . $status . "</span>";
            return $data;
         });

         $datatables->add('action', function($data){
            $id = trim($data['id']);
            $f_pinjam_cancel = $data['f_pinjam_cancel'];
            $d_pengembalian = $data['d_pengembalian'];
            $dfrom = $data['dfrom'];
            $dto = $data['dto'];
            $i_buku = $data['i_buku'];
            $data = '';
            $data      .= "<a href='" . base_url() . $this->folder . '/view/' . encrypt_url($id) . '/' . encrypt_url($dfrom) . '/' . encrypt_url($dto) . '/' . encrypt_url($i_buku) . "' title='View Data'><i class='fa fa-eye fa-lg warning darken-4 mr-1'></i></a>";
            if(check_role($this->id_menu, 3) && $f_pinjam_cancel == 'f'){
                $data .= "<a href='" . base_url() . $this->folder . '/edit/' . encrypt_url($id) . '/' . encrypt_url($dfrom) . '/' . encrypt_url($dto) . '/' . encrypt_url($i_buku) . "' title ='Edit Data'><i class='fa fa-pencil-square success darken-4 fa-lg mr-1'></i></a>";
            }
            if(check_role($this->id_menu, 4) && $f_pinjam_cancel == 'f'){
                $data      .= "<a href='#' onclick='sweetdeletetesa(\"" . $this->folder . "\",\"" . $id . "\",\"" . encrypt_url($dfrom)  . "\",\"" . encrypt_url($dto)  . '/' . encrypt_url($i_buku) . "\");' title='Hapus Data'><i class='fa fa-trash fa-lg red darken-4'></i></a>";
            } 
            if(check_role($this->id_menu, 5) && $f_pinjam_cancel == 'f') {
                $data      .= "<a href='#' onclick='openLink(\"" . $this->folder . "\",\"" . encrypt_url($id) . "\"); return false;' title='Print Data'><i class='fa fa-print fa-lg blue darken-4 mr-1'></i></a>";
            }

            return $data;
         });
         $datatables->hide('dfrom');
         $datatables->hide('dto');
         $datatables->hide('i_buku');
         $datatables->hide('i_company');
         return $datatables->generate();
    }

    public function get_buku($cari){
        return $this->db->query("SELECT
        DISTINCT
            a.i_buku,
            i_buku_id,
            initcap(e_buku_name) AS e_buku_name
        FROM
            buku a
        WHERE
            (e_buku_name ILIKE '%$cari%' OR i_buku_id ILIKE '%$cari%')
            AND a.i_company = '$this->i_company'
            AND f_buku_active = true
        ORDER BY 3 ASC
        ", FALSE);
    }

    public function get_mahasiswa($cari){
        return $this->db->query("SELECT
        DISTINCT
            a.i_name, 
            a.i_name_id, 
            initcap(e_name) AS e_name
        FROM
            tesa a
        WHERE
            (e_name ILIKE '%$cari%' OR i_name_id ILIKE '%$cari%')
            AND i_company = '$this->i_company'
            AND f_name_active = 'true'
        ORDER BY 3 ASC
        ", FALSE);
    }

    public function get_mahasiswa_detail($imahasiswa){
       
        return $this->db->query("SELECT
                a.i_name,
                a.i_name_id, 
                a.e_jurusan,
                a.e_prodi,
                a.e_alamat,
                a.e_umur,
                a.e_jk
            FROM
                tesa a
            WHERE i_name = $imahasiswa
        ", FALSE);
    }


    // public function get_nama_buku($cari){
    //     return $this->db->query("SELECT
    //     DISTINCT
    //         a.i_buku,
    //         i_buku_id,
    //         initcap(e_buku_name) AS e_buku_name
    //     FROM
    //         buku a
    //     WHERE
    //         (e_buku_name ILIKE '%$cari%' OR i_buku_id ILIKE '%$cari%')
    //         AND a.i_company = '$this->i_company'
    //         AND f_buku_active = true
    //     ORDER BY 3 ASC
    //     ", FALSE);
    // }

    public function get_detail_buku($ibuku){
        
        return $this->db->query("SELECT
            a.i_buku,
            a.e_pengarang,
            a.e_penerbit,
            a.n_tahun_terbit,
            a.harga_buku
        FROM
            buku a
        WHERE
            i_buku = '$ibuku'
        ", FALSE);
    } 

    public function running_number($thbl, $tahun)
    {
        $cek = $this->db->query("SELECT 
                substring(i_pinjam_id, 1, 3) AS kode 
            FROM peminjaman
            WHERE i_company = '$this->i_company'
            AND f_pinjam_cancel = 'f'
            ORDER BY i_pinjam_id DESC
            ");
        if ($cek->num_rows() > 0) {
            $kode = $cek->row()->kode;
        } else {
            $kode = 'PMN';
        }
        $query  = $this->db->query("SELECT
                max(substring(i_pinjam_id, 10, 6)) AS max
            FROM
                peminjaman
            WHERE to_char (d_pinjam, 'yyyy') >= '$tahun'
            AND i_company = '$this->i_company'
            AND f_pinjam_cancel = 'f'
            AND substring(i_pinjam_id, 1, 3) = '$kode'
            AND substring(i_pinjam_id, 5, 2) = substring('$thbl',1,2)
        ", false);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $no = $row->max;
            }
            $number = $no + 1;
            settype($number, "string");
            $n = strlen($number);
            while ($n < 6) {
                $number = "0" . $number;
                $n = strlen($number);
            }
            $number = $kode . "-" . $thbl . "-" . $number;
            return $number;
        } else {
            $number = "00001";
            $nomer  = $kode . "-" . $thbl . "-" . $number;
            return $nomer;
        }
    }

    public function cek_code(){
        $i_document = str_replace("_", "", $this->input->post('i_document'));
        return $this->db->query("SELECT
                i_pinjam_id
            FROM
                peminjaman
            WHERE
                upper(trim(i_pinjam_id)) = upper(trim('$i_document'))
                AND i_company = '$this->i_company'
        ", FALSE);
    }

    public function save(){        
        $query = $this->db->query("SELECT max(i_pinjam)+1 AS id FROM peminjaman", TRUE);
        if ($query->num_rows() > 0) {
            $id = $query->row()->id;
            if ($id == null) {
                $id = 1;
            } else {
                $id = $id;
            }
        } else {
            $id = 1;
        }

        // var_dump($id);
        // die;

        $ym = date('ym', strtotime($this->input->post('d_document')));
        $Y = date('Y', strtotime($this->input->post('d_document')));
        $header = array(
            'i_company'         => $this->session->i_company,
            'i_name'            => $this->input->post('i_name'),
            'i_pinjam'          => $id,
            'd_pinjam'          => $this->input->post('d_document'),
            'd_pengembalian'    => $this->input->post('d_kembali'),
            'd_pinjam_entry'    => current_datetime(),
            'f_pinjam_cancel'   => 'f',
            'e_remark'          => $this->input->post('e_remark'),
            'v_nilai_bersih'    => str_replace(",", "",$this->input->post('subtotal')),
            'i_pinjam_id'       => strtoupper($this->input->post('i_document')),
        );
        $this->db->insert('peminjaman', $header);
        if ($this->input->post('jml') > 0) {
            $i = 0;
            if (is_array($this->input->post('i_buku')) || is_object($this->input->post('i_buku'))) {
                foreach ($this->input->post('i_buku') as $i_buku) {
                    $item = array(
                        'i_pinjam'          => $id,
                        'i_buku'            => $i_buku,
                        'e_buku_name'       => $this->input->post('i_buku')[$i],
                        'n_harga'           => $this->input->post('harga_buku')[$i],
                        'n_quantity'        => $this->input->post('n_quantity')[$i],
                        'e_remark_item'     => $this->input->post('e_remark_item')[$i],
                        'n_diskon'          => $this->input->post('n_diskon')[$i],
                        'n_item_no'         => $i,
        
                    );
                    $this->db->insert('peminjaman_item', $item);
                    $i++;
                }
            }
        } else {
            die;
        }
    }

    public function get_data($id){
        return $this->db->query("SELECT
                a.*,
                d.e_name,
                d.e_prodi,
                d.e_alamat,
                d.e_jurusan,
                d.e_jk,
                d.e_umur
            FROM 
                peminjaman a
            INNER JOIN tesa d ON
                (d.i_name = a.i_name)
            WHERE i_pinjam = '$id'
        ", FALSE);
    }

    public function get_data_detail($id){
        return $this->db->query("SELECT
        a.*,
        c.i_buku_id,
        c.e_pengarang,
        c.e_penerbit,
        c.n_tahun_terbit,
        c.e_buku_name,
        c.harga_buku
        FROM
            peminjaman_item a
        INNER JOIN peminjaman b ON (b.i_pinjam = a.i_pinjam)
        INNER JOIN buku c ON (a.i_buku = c.i_buku)
        WHERE 
            a.i_pinjam = '$id'
        ", FALSE);
    }

    public function cek_edit(){
        $i_document = str_replace("_", "", $this->input->post('i_document'));
        $i_document_old = str_replace("_", "", $this->input->post('i_document_old'));
        return $this->db->query("SELECT
            i_pinjam_id
        FROM
            peminjaman
        WHERE
            trim(upper(i_pinjam_id)) <> trim(upper('$i_document_old'))
            AND trim(upper(i_pinjam_id)) = trim(upper('$i_document'))
            AND i_company = '$this->i_company'        
        ", FALSE); 
    }

    public function update(){
        $id = $this->input->post('id');
        $header = array(
            'i_company'         => $this->session->i_company,
            'i_name'            => $this->input->post('i_name'),
            // 'i_pinjam'          => $id,
            'd_pinjam'          => $this->input->post('d_document'),
            'd_pengembalian'    => $this->input->post('d_kembali'),
            'd_pinjam_update'   => current_datetime(),
            'f_pinjam_cancel'   => 'f',
            'e_remark'          => $this->input->post('e_remark'),
            'v_nilai_bersih'    => str_replace(",", "",$this->input->post('subtotal')),
            'i_pinjam_id'       => strtoupper($this->input->post('i_document')),
            'v_nilai_bersih'    => '0',
        );
        $this->db->where('i_pinjam', $id);
        $this->db->update('peminjaman', $header);
        $jml = $this->input->post('jml');
        if ($this->input->post('jml') > 0) {
            $i = 0;
            if (is_array($this->input->post('i_buku')) || is_object($this->input->post('i_buku'))) {
                $this->db->where('i_pinjam', $id);
                $this->db->delete('peminjaman_item');
                foreach ($this->input->post('i_buku') as $i_buku) {
                    $item = array(
                        'i_pinjam'          => $id,
                        'i_buku'            => $i_buku,
                        'e_buku_name    '   => $this->input->post('i_buku')[$i],
                        'n_harga'           => $this->input->post('harga_buku')[$i],
                        'n_quantity'        => $this->input->post('n_quantity')[$i],
                        'e_remark_item'     => $this->input->post('e_remark_item')[$i],
                        'n_diskon'          => $this->input->post('n_diskon')[$i],
                        'n_item_no'         => $i,
                    );
                    $this->db->insert('peminjaman_item', $item);
                    $i++;
                }
            }
        } else {
            die;
        }
    }

    public function cancel($id)
    {
        $table = array(
            'f_pinjam_cancel' => 't',
        );
        $this->db->where('i_pinjam', $id);
        $this->db->update('peminjaman', $table);
    }

    // public function update_print(){
    //     $this->db->query("UPDATE
    //         peminjaman SET
    //     ")
    // }
}