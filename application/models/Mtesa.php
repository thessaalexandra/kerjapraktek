<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\CodeigniterAdapter;

class Mtesa extends CI_Model{

    public function serverside(){
        $datatables = new Datatables(new CodeigniterAdapter);
        $datatables->query("SELECT
        i_name,
        i_name_id,
        e_name,
        e_jurusan,
        e_prodi,
        e_alamat,
        e_umur,
        e_jk,
        f_name_active FROM tesa WHERE i_company = '$this->i_company'
        ", FALSE);

        $datatables->edit('f_name_active', function($data){
            $id = $data['f_name_active'];
            if($data['f_name_active'] == 't'){
                $status = $this->lang->line('Aktif');
                $color = 'teal';
            }else{
                $status = $this->lang->line('Batal');
                $color = 'red';
            }
            // $data = "<button class = 'btn btn-outline-" . $color . "btn-sm round 'onclick =' changestatus(\"" . $this->folder . "\", \"" . $id . "\"); '>" . $status . "</button";
            $data = "<span class='badge bg-" . $color . " badge-pill'>" . $status . "</span>";
            return $data;
        });

       
        if(check_role($this->id_menu, 3)){
            $datatables->add('action', function($data){
                $id = trim($data['i_name']);
                $f_name_active = $data['f_name_active'];
                $data = '';
                $data .= "<a href='" . base_url() . $this->folder . '/view/' . encrypt_url($id) . "' title='View Data'><i class='fa fa-eye fa-lg warning darken-4 mr-1'></i></a>";
                if(check_role($this->id_menu, 3) && $f_name_active == 't'){
                    $data .= "<a href='" . base_url() . $this->folder . '/edit/' . encrypt_url($id) . "' title ='Edit Data'><i class='fa fa-pencil-square success darken-4 fa-lg mr-1'></i></a>";
                }
                if(check_role($this->id_menu, 4) && $f_name_active == 't'){
                    $data      .= "<a href='#' onclick='sweetdelete(\"" . $this->folder . "\",\"" . $id . "\");' title='Hapus Data'><i class='fa fa-trash fa-lg red darken-4'></i></a>";
                }    
                return $data;
            });
        }
        return $datatables->generate();
    }

    public function changestatus($id){
        $this->db->select('f_name_active');
        $this->db->from('tesa');
        $this->db->where('i_name', $id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $status = $query->row()->f_name_active;
        }else{
            $status = 'f';
        }
        if($status == 'f'){
            $fstatus = 't';
        }else{
            $fstatus = 'f';
        }
        $table = array(
            'f_name_active' =>$status,
        );
        $this->db->where('i_name', $id);
        $this->db->update('tesa', $table);
    }

    public function cek($kode){
        return $this->db->query("SELECT
        i_name_id FROM tesa WHERE trim(upper(i_name_id)) = trim(upper('$kode'))
        AND i_company = '$this->i_company'
        ", FALSE);
    }

    public function save($kode, $nama, $jurusan, $prodi, $alamat, $umur, $jk){
        $table = array(
            'i_company' => $this->i_company,
            'i_name_id' => $kode,
            'e_name' => $nama,
            'e_jurusan' => $jurusan,
            'e_prodi' => $prodi,
            'e_alamat' => $alamat,
            'e_umur' => $umur,
            'e_jk' => $jk,
            'd_name_entry' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('tesa', $table);
    }

    public function getdata($id){
        return $this->db->query("SELECT
        * FROM tesa WHERE i_name = '$id'
        ", FALSE);
    }

    public function cek_edit($kode, $kodeOld){
        return $this->db->query("SELECT
        i_name_id FROM tesa 
        WHERE 
        trim(upper(i_name_id)) <> trim(upper('$kodeOld')) AND
        trim(upper(i_name_id)) = trim(upper('$kode')) AND
        i_company = '$this->i_company'
        ", FALSE);
    }

    public function update($id, $kode, $nama, $jurusan, $prodi, $alamat, $umur, $jk){
        $table = array(
            'i_name_id' => $kode,
            'e_name' => $nama,
            'e_jurusan' => $jurusan,
            'e_prodi' => $prodi,
            'e_alamat' => $alamat,
            'e_umur' => $umur,
            'e_jk' => $jk,
            'd_name_update' => date('Y-m-d H:i:s'),
        );
        $this->db->where('i_name', $id);
        $this->db->update('tesa', $table);
    }

    public function cancel($id){
        $table = array(
            'f_name_active' => 'f',
        );
        $this->db->where('i_name', $id);
        $this->db->update('tesa', $table);
    }
}