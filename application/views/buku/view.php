<div class="content-header row">
    <div class="content-header-left col-md-12 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#" onclick="return false;"><?= $this->lang->line('Master'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?= base_url($this->folder); ?>"><?= $this->lang->line($this->title); ?></a></li>
                    <li class="breadcrumb-item active"><?= $this->lang->line('Detail'); ?> <?= $this->lang->line($this->title); ?></li>
                </ol>
            </div>
        </div>
</div>
</div>
<div class="content-body">
    <!-- Alternative pagination table -->
    <section id="pagination">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header header-elements-inline <?= $this->session->e_color; ?> bg-darken-1 text-white">
                        <h4 class="card-title"><i class="icon-eye"></i> <?= $this->lang->line('Detail'); ?> <?= $this->lang->line($this->title); ?></h4>
                        <input type="hidden" id="path" value="<?= $this->folder; ?>">
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <form class="form-validation" novalidate>
                                <div class="form-group">
                                    <label><?= $this->lang->line('Kode Buku'); ?> :</label>
                                    <div class="controls">
                                        <input type="text" readonly class="form-control round text-uppercase" value="<?=$data->i_buku_id; ?>" data-validation-required-message="<?= $this->lang->line('Required'); ?>" placeholder="<?= $this->lang->line('Masukan'); ?> <?= $this->lang->line('Kode Buku'); ?>" id="kode_buku" name="kode_buku" maxlength="15" autocomplete="off" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><?= $this->lang->line('Nama Buku'); ?> :</label>
                                    <div class="controls">
                                        <input type="text" readonly class="form-control round text-capitalize" value="<?=$data->e_buku_name; ?>" data-validation-required-message="<?= $this->lang->line('Required'); ?>" placeholder="<?= $this->lang->line('Masukan'); ?> <?= $this->lang->line('Nama Buku'); ?>" id="nama_buku" name="nama_buku" maxlength="40" autocomplete="off" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><?= $this->lang->line('Pengarang'); ?> :</label>
                                    <div class="controls">
                                        <input type="text" readonly class="form-control round text-capitalize" value="<?=$data->e_pengarang; ?>" data-validation-required-message="<?= $this->lang->line('Required'); ?>" placeholder="<?= $this->lang->line('Masukan'); ?> <?= $this->lang->line('Pengarang'); ?>" id="pengarang" name="pengarang" maxlength="40" autocomplete="off" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><?= $this->lang->line('Penerbit'); ?> :</label>
                                    <div class="controls">
                                        <input type="text" readonly class="form-control round text-capitalize" value="<?=$data->e_penerbit; ?>" data-validation-required-message="<?= $this->lang->line('Required'); ?>" placeholder="<?= $this->lang->line('Masukan'); ?> <?= $this->lang->line('Penerbit'); ?>" id="penerbit" name="penerbit" maxlength="40" autocomplete="off" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><?= $this->lang->line('Tahun Terbit'); ?> :</label>
                                    <div class="controls">
                                        <input type="number" readonly class="form-control round text-capitalize" value="<?= $data->n_tahun_terbit; ?>" data-validation-number-message="<?= $this->lang->line('Number'); ?>" placeholder="<?= $this->lang->line('Masukan'); ?> <?= $this->lang->line('Tahun Terbit'); ?>" id="tahun_terbit" name="tahun_terbit" autocomplete="off" required autofocus>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-start align-items-center">
                                    <a href="<?= base_url($this->folder); ?>" class="btn btn-secondary round btn-min-width mr-1"><i class="icon-action-undo mr-1"></i><?= $this->lang->line('Kembali'); ?></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Alternative pagination table -->
</div>