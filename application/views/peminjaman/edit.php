<style>
    .table.table-xs th,
    .table td,
    .table.table-xs td {
        padding: 0.3rem 0.3rem;
    }

    .table>tfoot>tr>th,
    .table>tfoot>tr>td {
        border: none !important;
    }
</style>
<form class="form-validation" novalidate>
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" onclick="return false;"><?= $this->lang->line('Peminjaman'); ?></a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url($this->folder . '/index/' . encrypt_url($dfrom) . '/' . encrypt_url($dto) . '/' . encrypt_url($hbuku)); ?>"><?= $this->lang->line($this->title); ?></a></li>
                        <li class="breadcrumb-item active"><?= $this->lang->line('Ubah'); ?> <?= $this->lang->line($this->title); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="pagination">
            <div class="row">
                <div class="col-12">
                    <div class="card box-shadow-0 border-primary">
                        <div class="card-header card-head-inverse <?= $this->session->e_color; ?> bg-darken-1 text-white">
                            <h4 class="card-title"><i class="icon-pencil"></i> <?= $this->lang->line('Ubah'); ?> <?= $this->lang->line($this->title); ?></h4>
                            <input type="hidden" id="path" value="<?= $this->folder; ?>">
                            <input type="hidden" id="d_from" value="<?= encrypt_url($dfrom); ?>">
                            <input type="hidden" id="d_to" value="<?= encrypt_url($dto); ?>">
                            <input type="hidden" id="hbuku" value="<?= encrypt_url($hbuku); ?>">
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <!-- Baris ke 1 -->
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label><?= $this->lang->line("Nomor Dokumen"); ?> :</label>
                                            <fieldset>
                                                <div class="input-group input-group-sm">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <span class="fa fa-hashtag"></span>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="id" id="id" readonly value="<?= $data->i_pinjam; ?>">
                                                    <input type="hidden" name="i_document_old" id="i_document_old" readonly value="<?= $data->i_pinjam_id; ?>">
                                                    <input type="text" name="i_document" id="i_document" readonly value="<?= $data->i_pinjam_id; ?>" placeholder="PMJ-<?= date('ym'); ?>-00001" class="form-control form-control-sm text-uppercase" data-validation-required-message="<?= $this->lang->line("Required"); ?>" maxlength="500" autocomplete="off" required aria-label="Text input with checkbox">
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label><?= $this->lang->line("Tanggal Dokumen"); ?> :</label>
                                            <div class="input-group input-group-sm controls">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="fa fa-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="date" class="form-control form-control-sm date" readonly min="<?= date('Y-m-01'); ?>" max="<?= date('Y-m-d'); ?>" value="<?= $data->d_pinjam; ?>" data-validation-required-message="<?= $this->lang->line("Required"); ?>" placeholder="" id="d_document" name="d_document" maxlength="500" autocomplete="off" required>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Tanggal Pengembalian:</label>
                                            <div class="input-group input-group-sm controls">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="fa fa-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="date" class="form-control form-control-sm date" min="<?= date('Y-m-d'); ?>" value="<?= $data->d_pengembalian;?>" data-validation-required-message="<?= $this->lang->line("Required"); ?>" placeholder="" id="d_kembali" name="d_kembali" autocomplete="off" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Baris ke 2 -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?= $this->lang->line("Nama Mahasiswa"); ?> :</label>
                                            <div class="controls">
                                                <select class="form-control" name="i_name" id="i_name" data-placeholder="<?= $this->lang->line("Pilih") . ' ' . $this->lang->line("Mahasiswa"); ?>" required data-validation-required-message="<?= $this->lang->line("Required"); ?>">
                                                    <option value="<?= $data->i_name; ?>"><?= $data->e_name; ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('Alamat Mahasiswa'); ?> :</label>
                                            <div class="controls">
                                                <textarea class="form-control text-capitalize clear" placeholder="<?= $this->lang->line('Alamat Mahasiswa'); ?>" id="alamat" name="alamat" autocomplete="off" readonly><?= $data->e_alamat; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Bari ke 3 -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label><?= $this->lang->line('Jurusan'); ?> :</label>
                                                <div class="controls">
                                                    <input type="text" class="form-control clear form-control-sm text-capitalize" placeholder="<?= $this->lang->line('Jurusan'); ?>" id="jurusan" name="jurusan" autocomplete="off" readonly value="<?= $data->e_jurusan; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label><?= $this->lang->line('Prodi'); ?> :</label>
                                                <div class="controls">
                                                    <input type="text" class="form-control clear form-control-sm text-capitalize" placeholder="<?= $this->lang->line('Prodi'); ?>" id="prodi" name="prodi" autocomplete="off" readonly value="<?= $data->e_prodi; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label><?= $this->lang->line('Umur'); ?> :</label>
                                                <div class="controls">
                                                    <input type="number" class="form-control clear form-control-sm text-capitalize" data-validation-required-message="<?= $this->lang->line('Required'); ?>" placeholder="<?= $this->lang->line('Umur'); ?>" id="umur" name="umur" autocomplete="off" readonly value="<?= $data->e_umur; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label><?= $this->lang->line('Jenis Kelamin'); ?> :</label>
                                                <div class="controls">
                                                    <input type="text" class="form-control clear form-control-sm text-capitalize" data-validation-required-message="<?= $this->lang->line('Required'); ?>" placeholder="<?= $this->lang->line('Jenis Kelamin'); ?>" id="jk" name="jk" autocomplete="off" readonly value="<?= $data->e_jk; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Baris ke 5 -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line("Keterangan"); ?> :</label>
                                            <textarea required class="form-control text-capitalize" placeholder="<?= $this->lang->line('Keterangan'); ?>" id="e_remark" name="e_remark"><?= $data->e_remark; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/ Alternative pagination table -->
    </div>
    <div class="content-body">
        <!-- Alternative pagination table -->
        <section id="pagination">
            <div class="row">
                <div class="col-12">
                    <div class="card box-shadow-0 border-primary">
                        <div class="card-header card-head-inverse <?= $this->session->e_color; ?> bg-darken-1 text-white">
                            <h4 class="card-title"><i class="fa fa-cart-arrow-down"></i> <?= $this->lang->line("Detail"); ?> <?= $this->lang->line("Pesanan Sales"); ?></h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <div class="form-body">
                                    <div class="table-responsive">
                                        <table class="table table-xs table-column table-bordered" id="tabledetail">
                                            <thead class="<?= $this->session->e_color; ?> bg-darken-1 text-white">
                                                <tr>
                                                <th class="text-center" width="3%">No</th>
                                                <th class="text-center" width="15%" valign="center"><?= $this->lang->line("Nama Buku"); ?></th>
                                                    <th class="text-center" width="14%" valign="center"><?= $this->lang->line("Pengarang"); ?></th>
                                                    <th class="text-center" width="13%" valign="center"><?= $this->lang->line("Penerbit"); ?></th>
                                                    <th class="text-center" width="12%" valign="center"><?= $this->lang->line("Tahun Terbit"); ?></th>
                                                    <th class="text-center" width="10%" valign="center"><?= $this->lang->line("Harga"); ?></th>
                                                    <th class="text-center" width="7%" valign="center"><?= $this->lang->line("Qty"); ?></th>
                                                    <th class="text-center" width="7%"><?= $this->lang->line("Diskon"); ?></th>
                                                    <th class="text-center" width="9%" valign="center"><?= $this->lang->line("Total"); ?></th>
                                                    <th class="text-center" width="15%" valign="center"><?= $this->lang->line("Keterangan"); ?></th>
                                                    <th class="text-center" width="3%"><i class="fa fa-plus-circle fa-lg" title="<?= $this->lang->line('Ubah'); ?>" id="addrow"></i></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $i = 0;
                                                if ($detail->num_rows() > 0) {
                                                    foreach ($detail->result() as $key) {
                                                        $i++; ?>
                                                        <tr>
                                                            <td class="text-center">
                                                                <spanx id="snum<?= $i; ?>"><?= $i; ?></spanx>
                                                            </td>
                                                            <td>
                                                                <select data-nourut="<?= $i; ?>" required class="form-control select2-size-sm" name="i_buku[]" id="i_buku<?= $i; ?>">
                                                                    <option value="<?= $key->i_buku; ?>"><?= $key->i_buku_id . ' - ' . $key->e_buku_name; ?></option>
                                                                </select>
                                                            </td>
                                                            <td><input type="text" class="form-control text-right form-control-sm" id="e_pengarang<?= $i; ?>" name="e_pengarang[]" readonly value="<?= $key->e_pengarang; ?>"></td>
                                                            <td><input type="text" class="form-control text-right form-control-sm" id="e_penerbit<?= $i; ?>" name="e_penerbit[]"  readonly value="<?= $key->e_penerbit; ?>"></td>
                                                            <td><input type="text" class="form-control text-right form-control-sm" id="n_tahun_terbit<?= $i; ?>" name="n_tahun_terbit[]" readonly value="<?= $key->n_tahun_terbit; ?>"></td>
                                                            <td><input type="text" class="form-control text-right form-control-sm" id="harga_buku<?= $i; ?>" name="harga_buku[]" readonly value="<?= $key->n_harga; ?>"></td>
                                                            <td><input type="text" class="form-control text-right form-control-sm" id="n_quantity<?= $i; ?>" name="n_quantity[]" value="<?= $key->n_quantity; ?>"></td>
                                                            <td><input type="text" class="form-control text-right form-control-sm" id="n_diskon<?= $i; ?>" name="n_diskon[]" value="<?= $key->n_diskon; ?>"></td>
                                                            <td><input type="text" class="form-control text-right form-control-sm" id="nilai_bersih<?= $i; ?>" name="nilai_bersih[]" readonly value="<?= $key->n_harga; ?>"></td>
                                                            <td><input type="text" class="form-control form-control-sm" id="e_remark_item" name="e_remark_item[]" value="<?= $key->e_remark_item; ?>"></td>
                                                            <td class="text-center"><i title="Delete" class="fa fa-minus-circle fa-lg text-danger ibtnDel"></i></td>
                                                        </tr>
                                                <?php }
                                                } ?>
                                            </tbody>
                                            <input type="hidden" id="jml" name="jml" value="<?= $i; ?>">
                                        </table>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" id="submit" class="btn btn-warning round btn-min-width mr-1"><i class="icon-paper-plane mr-1"></i><?= $this->lang->line("Ubah"); ?></button>
                                    <a href="<?= base_url($this->folder . '/index/' . encrypt_url($dfrom) . '/' . encrypt_url($dto) . '/' . encrypt_url($hbuku)); ?>" class="btn btn-secondary round btn-min-width mr-1"><i class="icon-action-undo mr-1"></i><?= $this->lang->line("Kembali"); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</form>