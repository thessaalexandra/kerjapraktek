<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <title>Print <?= $this->title; ?></title>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/print.css">
</head>

<body>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="50%">
                <p class="header text-bold-700"><?= $this->lang->line('Surat Peminjaman Buku'); ?></p>
            </td>
        </tr>
        <tr>
            <?php if ($data->d_pinjam == null || $data->d_pinjam == '') { ?>
                <td><span class="judul font-weight-bold">Nomor Dokumen : <?= $data->i_pinjam_id; ?> ~ [BARU]</span></td>
            <?php } else { ?>
                <td><span class="judul font-weight-bold">Nomor Dokumen : <?= $data->i_pinjam_id; ?></span></td>
            <?php } ?>
        </tr>
        <tr>
            <td><span class="judul font-weight-bold">Tanggal Dokumen : <?= date('d', strtotime($data->d_pinjam)) . ' ' . bulan(date('m', strtotime($data->d_pinjam))) . ' ' . date('Y', strtotime($data->d_pinjam)); ?></span></td>
            <hr class="mt-0 mb-0">
        </tr>
        <tr>
            <td colspan="2">
                <hr class="mt-0 mb-0">
            </td>
        </tr>
        <tr>
            <!-- <td width="50%"><span class="header text-bold-700"><?= $company->e_company_name; ?></span></td> -->
            <?php if ($data->d_pinjam == null || $data->d_pinjam == '') { ?>
                <td class="text-right"><span class="text-bold-700 header">[BARU] ~ <?= '[' . $data->i_name_id . '] - ' . $data->e_name; ?></span></td>
            <?php } else { ?>
                <td class="text-right"><span class="text-bold-700 header"><?= '[' . $data->e_name . ']'; ?></span></td>
            <?php } ?>
        </tr>
        <tr>
            <!-- <td><span class="judul text-capitalize"><?= $company->e_company_address; ?></span></td> -->
            <td class="text-right"><span class="judul"><?= $data->e_alamat; ?></span></td>
        </tr>
        <tr>
            <!-- <td><span class="judul"><?php if ($company->e_company_npwp_code != null) { ?><?= $this->lang->line('NPWP'); ?> - <?= $company->e_company_npwp_code; ?><?php } ?></span></td> -->
            <td class="text-right"><span class="judul"><?= $data->e_jurusan; ?></span></td>
        </tr>
        <tr>
            <!-- <td><span class="judul"><?php if ($company->e_company_phone != null) { ?><?= $this->lang->line('Telepon'); ?> - <?= $company->e_company_phone; ?><?php } ?></span></td> -->
            <td colspan="2" class="text-right judul"><span><?= $data->e_prodi; ?></span></td>
        </tr>
        <tr>
            <td colspan="2"><span class="font-12">Dengan hormat,</span></td>
        </tr>
        <tr>
            <td colspan="2"><span class="font-12">Berikut merupakan surat peminjaman buku yaitu:</span></td>
        </tr>
    </table>
    <input type="hidden" id="id" value="<?= $data->i_pinjam; ?>">
    <input type="hidden" id="path" value="<?= base_url($this->folder); ?>">
    <table class="judul" width="100%" border="1" cellspacing="0" cellpadding="2">
        <thead>
            <tr>
                <th scope="col" class="text-center" width="3%">No</th>
                <th scope="col"><?= $this->lang->line('Kode Buku'); ?></th>
                <th scope="col"><?= $this->lang->line('Nama Buku'); ?></th>
                <th scope="col"><?= $this->lang->line('Pengarang'); ?></th>
                <th scope="col"><?= $this->lang->line('Penerbit'); ?></th>
                <th scope="col"><?= $this->lang->line('Tahun Terbit'); ?></th>
                <th scope="col"><?= $this->lang->line('Keterangan'); ?></th>
            </tr>
        </thead>
        <tbody>
        <?php $i = 0;
            if ($detail->num_rows() > 0) {
                foreach ($detail->result() as $key) {
                    $i++;
            ?>
                    <tr>
                        <td class="text-center"><?= $i; ?></td>
                        <td><?= $key->i_buku_id; ?></td>
                        <td><?= $key->e_buku_name; ?></td>
                        <td><?= $key->e_pengarang; ?></td>
                        <td><?= $key->e_penerbit; ?></td>
                        <td><?= $key->n_tahun_terbit; ?></td>
                        <td><?= $key->e_remark_item ?></td>
                    </tr>
            <?php
                }
            } ?>
        </tbody>
    </table>
    <table class="judul" width="100%" border="0" cellspacing="0" cellpadding="2">
        <tfoot>
            <tr>
                <?php if ($data->i_pinjam_id != '') {
                    $reff = $this->db->get_where('peminjaman', ['f_pinjam_cancel' => false, 'i_company' => $this->i_company])->row()->i_pinjam_id;
                } else {
                    $reff = '';
                } ?>
                <td width="62%"><strong>KETERANGAN : ( <?= $reff; ?> ) <?= $data->e_remark; ?></strong></td>
            </tr>
        </tfoot>
    </table>
    <table class="judul" width="100%" border="1" cellspacing="0" cellpadding="4">
        <tr>
            <td>
                <div class="card-content">
                    <span class="mb-1">
                        <P>Penyiapan :</P>
                    </span><br>
                </div>
            </td>
            <td>
                <div class="card-content">
                    <span class="mb-1">
                        <P>Cek Akhir :</P>
                    </span><br>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%;" class="judul">
        <tbody class="text-center">
            <tr>
                <td>
                    <span class="mb-2">Hormat kami</span>
                </td>
                <td>
                    <span class="mb-2">Disetujui</span>
                </td>
            </tr>
            <tr height="54px">
                <td></td>
            </tr>
            <tr>
                <td class="text-muted">
                    <p class="mt-3">( &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; )</p>
                </td>
                <td class="text-muted">
                    <p class="mt-3">( &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; )</p>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6"><span class="font-12">*Tanggal Cetak : <?= date('d') . ' ' . bulan(date('m')) . ' ' . date('Y') . ' ' . date('H:i:s'); ?></span></td>
            </tr>
            <tr>
                <td colspan="6" class="text-center"><a href="#" onclick="window.print();" class="button button1 no-print"> <i class="feather icon-printer mr-25 common-size"></i> <?= $this->lang->line("Cetak"); ?></a></td>
            </tr>
        </tfoot>
    </table>
    <?= put_footer(); ?>
</body>
<!-- END: Body-->
<!-- <script>
    window.addEventListener('afterprint', (event) => {
        console.log('After print');
    });

    window.onbeforeprint = function() {
        console.log('This will be called before the user prints.');
    };
    window.onafterprint = function() {
        console.log('This will be called after the user prints');
    };
</script> -->

</html>