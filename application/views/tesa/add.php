<div class="content-header row">
    <div class="content-header-left col-md-12 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#" onclick="return false;"><?= $this->lang->line('Master'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?= base_url($this->folder); ?>"><?= $this->lang->line($this->title); ?></a></li>
                    <li class="breadcrumb-item active"><?= $this->lang->line('Tambah'); ?> <?= $this->lang->line($this->title); ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <!-- Alternative pagination table -->
    <section id="pagination">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header header-elements-inline <?= $this->session->e_color; ?> bg-darken-1 text-white">
                        <h4 class="card-title"><i class="icon-plus"></i> <?= $this->lang->line('Tambah'); ?> <?= $this->lang->line($this->title); ?></h4>
                        <input type="hidden" id="path" value="<?= $this->folder; ?>">
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <form class="form-validation" novalidate>
                                <div class="form-group">
                                    <label><?= $this->lang->line('Kode Nama'); ?> :</label>
                                    <div class="controls">
                                        <input type="text" class="form-control round text-uppercase" data-validation-required-message="<?= $this->lang->line('Required'); ?>" placeholder="<?= $this->lang->line('Masukan'); ?> <?= $this->lang->line('Kode Nama'); ?>" id="kode" name="kode" maxlength="15" autocomplete="off" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><?= $this->lang->line('Nama Lengkap'); ?> :</label>
                                    <div class="controls">
                                        <input type="text" class="form-control round text-capitalize" data-validation-required-message="<?= $this->lang->line('Required'); ?>" placeholder="<?= $this->lang->line('Masukan'); ?> <?= $this->lang->line('Nama Lengkap'); ?>" id="nama" name="nama" maxlength="40" autocomplete="off" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><?= $this->lang->line('Jurusan'); ?> :</label>
                                    <div class="controls">
                                        <select class="form-control round select2" name="jurusan" id="jurusan" required data-placeholder="<?= $this->lang->line("Pilih") . ' ' . $this->lang->line("Jurusan"); ?>" data-validation-required-message="<?= $this->lang->line("Required"); ?>">
                                            <option value="00" <?php if ('00' == 'e_jurusan') {
                                                                    echo "selected";
                                                                } ?>>Pilih Jurusan</option>
                                            <option value="01" <?php if ('01' == 'e_jurusan') {
                                                                    echo "selected";
                                                                } ?>>S1 Informatika</option>
                                            <option value="02" <?php if ('02' == 'e_jurusan') {
                                                                    echo "selected";
                                                                } ?>>S1 Teknologi Informasi</option>
                                            <option value="03" <?php if ('03' == 'e_jurusan') {
                                                                    echo "selected";
                                                                } ?>>S1 Rekayasa Perangkat Lunak</option>
                                            <option value="04" <?php if ('04' == 'e_jurusan') {
                                                                    echo "selected";
                                                                } ?>>S1 MBTI</option>
                                            <option value="05" <?php if ('05' == 'e_jurusan') {
                                                                    echo "selected";
                                                                } ?>>S1 Akuntansi</option>
                                            <option value="06" <?php if ('06' == 'e_jurusan') {
                                                                    echo "selected";
                                                                } ?>>S1 Teknik Industri</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><?= $this->lang->line('Prodi'); ?> :</label>
                                    <div class="controls">
                                        <select class="form-control round select2" name="prodi" id="prodi" required data-placeholder="<?= $this->lang->line("Pilih") . ' ' . $this->lang->line("Prodi"); ?>" data-validation-required-message="<?= $this->lang->line("Required"); ?>">
                                            <option value="00" <?php if ('00' == 'e_jurusan') {
                                                                    echo "selected";
                                                                } ?>>Pilih Prodi</option>
                                            <option value="01" <?php if ('01' == 'e_prodi') {
                                                                    echo "selected";
                                                                } ?>>Informatika</option>
                                            <option value="02" <?php if ('02' == 'e_prodi') {
                                                                    echo "selected";
                                                                } ?>>Ekonomi dan Bisnis</option>
                                            <option value="03" <?php if ('03' == 'e_prodi') {
                                                                    echo "selected";
                                                                } ?>>Rekayasa Industri</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><?= $this->lang->line('Alamat'); ?> :</label>
                                    <div class="controls">
                                        <textarea class="form-control round text-capitalize" data-validation-required-message="<?= $this->lang->line('Required'); ?>" placeholder="<?= $this->lang->line('Masukan'); ?> <?= $this->lang->line('Alamat Pelanggan'); ?>" id="alamat" name="alamat" autocomplete="off" required autofocus> </textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><?= $this->lang->line('Umur'); ?> :</label>
                                    <div class="controls">
                                        <input type="number" onkeyup="usia();"  class="form-control round text-capitalize" data-validation-number-message="<?= $this->lang->line('Number'); ?>" placeholder="<?= $this->lang->line('Masukan'); ?> <?= $this->lang->line('Umur'); ?>" id="umur" name="umur" autocomplete="off" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><?= $this->lang->line('Jenis Kelamin'); ?> :</label>
                                    <div class="controls">
                                        <select class="form-control round select2" name="jk" id="jk" required data-placeholder="<?= $this->lang->line("Pilih") . ' ' . $this->lang->line("Prodi"); ?>" data-validation-required-message="<?= $this->lang->line("Required"); ?>">
                                            <option value="00" <?php if ('00' == 'e_jk') {
                                                                    echo "selected";
                                                                } ?>>Pilih Jenis Kelamin</option>
                                            <option value="01" <?php if ('01' == 'e_jk') {
                                                                    echo "selected";
                                                                } ?>>Perempuan</option>
                                            <option value="02" <?php if ('02' == 'e_jk') {
                                                                    echo "selected";
                                                                } ?>>Laki-Laki</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="d-flex justify-content-start align-items-center">
                                    <button type="submit" class="btn btn-success round btn-min-width mr-1"><i class="icon-paper-plane mr-1"></i><?= $this->lang->line('Simpan'); ?></button>
                                    <a href="<?= base_url($this->folder); ?>" class="btn btn-secondary round btn-min-width mr-1"><i class="icon-action-undo mr-1"></i><?= $this->lang->line('Kembali'); ?></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Alternative pagination table -->
</div>