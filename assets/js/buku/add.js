$(document).ready(function () {
    var controller = $("#path").val();
    var tu = $('#tahun_terbit').val();
    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    $('form').on('submit', function (e) { //bind event on form submit.
        e.preventDefault();
        var formData = new FormData(this);
        if (formData) {
            // if (t.val() == string.slice(0, 4)) {
            if (tu < 1000) {
                Swal.fire('MASUKAN TAHUN DENGAN BENAR !');
                $('#tahun_terbit').val(2023);
            } else {
                sweetaddthessa(controller, formData);
            }
            // } else {
            // Swal.fire('Tahun Terbit Tidak Boleh Lebih Dari 4 Karakter!');
            //     return false;
            // }
        }
    });
});

function tahun_terbit0() {
    var t = $('#tahun_terbit').val();
    if (t > 9999) {
        Swal.fire('MASUKAN TAHUN DENGAN BENAR !');
        $('#tahun_terbit').val(2023);
    }
}