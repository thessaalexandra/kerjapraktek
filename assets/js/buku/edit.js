$(document).ready(function () {
    var controller = $("#path").val();
    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    $('form').on('submit', function (e) { //bind event on form submit.
        e.preventDefault();
        var formData = new FormData(this);
        if (formData) {
            if (t.val() == string.slice(0, 4)) {
                sweeteditthessa(controller, formData);
            } else {
                Swal.fire('Tahun Terbit Tidak Boleh Lebih Dari 4 Karakter!');
                return false;
            }
        }
    });
});

function tahun_terbit() {
    var t = $('#tahun_terbit').val();
    let t = string.slice(0, 4);
    if (t != string.slice(0, 4)) {
        Swal.fire('Tahun Terbit Tidak Boleh Lebih Dari 4 Karakter!');
        $('#tahun_terbit').val(2023);
    }
}