$(document).ready(function () {
    number();

    $("#i_name").select2({
        width: "100%",
        dropdownAutoWidth: true,
        allowClear: true,
        ajax: {
            url: base_url + $("#path").val() + "/get_mahasiswa",
            dataType: "json",
            delay: 250,
            data: function (params) {
                var query = {
                    q: params.term,
                };
                return query;
            },
            processResults: function (data) {
                return {
                    results: data,
                };
            },
            cache: false,
        },
    })


    $("#i_buku").select2({
        dropdownAutoWidth: true,
        width: "100%",
        containerCssClass: "select-sm",
        allowClear: true,
        ajax: {
            url: base_url + $("#path").val() + "/get_buku",
            dataType: "json",
            delay: 250,
            data: function (params) {
                var query = {
                    q: params.term,
                };
                return query;
            },
            processResults: function (data) {
                return {
                    results: data,
                };
            },
            cache: false,
        },
    }).change(function (event) {
        $("#i_name").val("");
        $("#i_name").html("");
        $(".clear").val("");
        clear_tabel();
    });

    $("#i_name").select2({
        dropdownAutoWidth: true,
        width: "100%",
        containerCssClass: "select-sm",
        allowClear: true,
        ajax: {
            url: base_url + $("#path").val() + "/get_mahasiswa",
            dataType: "json",
            delay: 250,
            data: function (params) {
                var query = {
                    q: params.term,
                };
                return query;
            },
            processResults: function (data) {
                return {
                    results: data,
                };
            },
            cache: false,
        },
    }).change(function (event) {
        $.ajax({
            type: "post",
            data: {
                i_name: $(this).val(),
            },
            url: base_url + $("#path").val() + "/get_mahasiswa_detail",
            dataType: "json",
            success: function (data) {
                clear_tabel();
                if (data["header"] != null) {
                    $("#alamat").val(data["header"][0]["e_alamat"]);
                    $("#jurusan").val(data["header"][0]["e_jurusan"]);
                    $("#prodi").val(data["header"][0]["e_prodi"]);
                    $("#umur").val(data["header"][0]["e_umur"]);
                    $("#jk").val(data["header"][0]["e_jk"]);
                } else {
                    $(".clear").val("");
                    Swal.fire({
                        type: "error",
                        title: g_maaf,
                        text: "Non-existent data : (",
                        confirmButtonClass: "btn btn-danger",
                    });
                }
            },
            error: function () {
                Swal.fire({
                    type: "error",
                    title: g_maaf,
                    text: "500 internal server error : (",
                    confirmButtonClass: "btn btn-danger",
                });
            },
        });
    });

    $("#d_document").change(function () {
        number();
    });

    var Detail = $(function () {
        var i = $("#jml").val();
        $("#addrow").on("click", function () {
            i++;
            var no = $("#tabledetail tbody tr").length + 1;
            if (no <= 20) {
                $("#jml").val(i);
                var newRow = $("<tr>");
                var cols = "";
                cols += `<td class="text-center" valign="center"><spanx id="snum${i}">${no}</spanx></td>`;
                cols += `<td><select data-nourut="${i}" required class="form-control select2-size-sm" name="i_buku[]" id="i_buku${i}"><option value=""></option></select></td>`;
                cols += `<td><input type="text" class="form-control text-right form-control-sm" id="e_pengarang${i}" name="e_pengarang[]" readonly>`;
                cols += `<td><input type="text" class="form-control text-right form-control-sm" id="e_penerbit${i}" name="e_penerbit[]" readonly><input type="hidden" id="e_penerbit${i}" name="e_penerbit[]" readonly></td>`;
                cols += `<td><input type="text" class="form-control text-right form-control-sm" id="n_tahun_terbit${i}" name="n_tahun_terbit[]" readonly><input type="hidden" id="n_tahun_terbit${i}" name="n_tahun_terbit[]" readonly></td>`;

                cols += `<td><input type="text" class="form-control form-control-sm" name="e_ttb_remark[]"></td>`;
                cols += `<td class="text-center"><i title="Delete" class="fa fa-minus-circle fa-lg text-danger ibtnDel"></i></td>`;
                newRow.append(cols);
                $("#tabledetail").append(newRow);
                $("#i_buku" + i).select2({
                    placeholder: g_pilihdata,
                    dropdownAutoWidth: true,
                    width: "100%",
                    containerCssClass: "select-xs",
                    allowClear: true,
                    ajax: {
                        url: base_url + $("#path").val() + "/get_buku",
                        dataType: "json",
                        delay: 250,
                        data: function (params) {
                            var query = {
                                q: params.term,
                            };
                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: data,
                            };
                        },
                        cache: false,
                    },
                }).change(function (event) {
                    var z = $(this).data("nourut");
                    var ada = false;
                    for (var x = 1; x <= $("#jml").val(); x++) {
                        if ($(this).val() != null) {
                            if ($(this).val() == $("#i_buku" + x).val() && z != x) {
                                Swal.fire({
                                    type: "error",
                                    title: g_maaf,
                                    text: g_exist,
                                    confirmButtonClass: "btn btn-danger",
                                });
                                ada = true;
                                break;
                            }
                        }
                    }
                    if (ada) {
                        $(this).val("");
                        $(this).html("");
                    } else {
                        $.ajax({
                            type: "post",
                            data: {
                                i_buku: $(this).val(),
                            },
                            url: base_url + $("#path").val() + "/get_detail_buku",
                            dataType: "json",
                            success: function (data) {
                                $("#pengarang" + z).val(data.detail[0]['e_pengarang']);
                                $("#penerbit" + z).val(data.detail[0]['e_penerbit']);
                                $("#tahun_terbit" + z).val(data.detail[0]['n_tahun_terbit']);
                            },
                            error: function () {
                                Swal.fire({
                                    type: "error",
                                    title: g_maaf,
                                    text: "500 internal server error : (",
                                    confirmButtonClass: "btn btn-danger",
                                });
                            },
                        });

                    }

                });
            };

            $("#tabledetail").on("click", ".ibtnDel", function (event) {
                $(this).closest("tr").remove();

                $("#jml").val(i);
                var obj = $("#tabledetail tr:visible").find("spanx");
                $.each(obj, function (key, value) {
                    id = value.id;
                    $("#" + id).html(key + 1);
                });

            });
        });

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        $("form").on("submit", function (e) {
            //bind event on form submit.
            let tabel = $("#tabledetail tbody tr").length;
            let ada = false;
            if (tabel < 1) {
                Swal.fire(g_maaf, g_detailmin, "error");
                return false;
            }

            $("#tabledetail tbody tr").each(function () {
                $(this).find("td select").each(function () {
                    if ($(this).val() == "" || $(this).val() == null) {
                        Swal.fire("Tidak boleh kosong!");
                        ada = true;
                    }
                });
                $(this).find("td .n_quantity").each(function () {
                    if (
                        $(this).val() == "" ||
                        $(this).val() == null ||
                        $(this).val() == 0
                    ) {
                        Swal.fire("Tidak Boleh Kosong Atau 0!");
                        ada = true;
                    }
                });
            });

        });
    });

    function clear_tabel() {
        $("#tabledetail tbody").empty();
        $(".tfoot").val(0);
        $("#jml").val("0");
    }

    function number() {
        $.ajax({
            type: "post",
            data: {
                tanggal: $("#d_document").val(),
            },
            url: base_url + $("#path").val() + "/number ",
            dataType: "json",
            success: function (data) {
                $("#i_document").val(data);
            },
            error: function () {

            },
        });
    }
});