$(document).ready(function () {
    hitung();
    for (let i = 1; i <= $('#jml').val(); i++) {
        $("#i_name" + i).select2({
            placeholder: "Search",
            dropdownAutoWidth: true,
            width: '100%',
            containerCssClass: 'select-xs',
            allowClear: true,
            ajax: {
                url: base_url + $("#path").val() + "/get_mahasiswa",
                dataType: "json",
                delay: 250,
                data: function (params) {
                    var query = {
                        q: params.term,
                        i_pinjam: $("#i_pinjam").val(),
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data,
                    };
                },
                cache: false,
            },
        }).change(function (event) {
            var z = $(this).data("nourut");
            var ada = false;
            for (var x = 1; x <= $("#jml").val(); x++) {
                if ($(this).val() != null) {
                    if ($(this).val() == $("#i_name" + x).val() /* && $("#i_level" + z).val() == $("#i_level" + x).val() */ && z != x) {
                        Swal.fire({
                            type: "error",
                            title: g_maaf,
                            text: g_exist,
                            confirmButtonClass: "btn btn-danger",
                        });
                        ada = true;
                        break;
                    }
                }
            }
            if (ada) {
                $(this).val("");
                $(this).html("");
            } else {
                $.ajax({
                    type: "post",
                    data: {
                        'i_name': $('#i_name' + z).val(),
                    },
                    url: base_url + $("#path").val() + '/get_mahasiswa_detail',
                    dataType: "json",
                    success: function (data) {
                        $("#alamat" + z).val(data.detail[0]['e_alamat']);
                        $("#jurusan" + z).val(data.detail[0]['e_jurusan']);
                        $("#prodi" + z).val(data.detail[0]['e_prodi']);
                        $("#umur" + z).val(data.detail[0]['e_umur']);
                        $("#jk" + z).val(data.detail[0]['e_jk']);
                    },
                    error: function () {
                        Swal.fire({
                            type: "error",
                            title: g_maaf,
                            text: "Error data",
                            confirmButtonClass: "btn btn-danger",
                        });
                    }
                });
            }
        });
    }
    for (let i = 1; i <= $('#jml').val(); i++) {
        $("#i_pinjam_item" + i).select2({
            placeholder: g_pilihdata,
            dropdownAutoWidth: true,
            width: '100%',
            containerCssClass: 'select-xs',
            allowClear: true,
            ajax: {
                url: base_url + $("#path").val() + "/get_pinjam",
                dataType: "json",
                delay: 250,
                data: function (params) {
                    var query = {
                        q: params.term,
                        i_name: $("#i_name").val(),
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data,
                    };
                },
                cache: false,
            },
        }).change(function (event) {
            var z = $(this).data("nourut");
            var ada = false;
            for (var x = 1; x <= $("#jml").val(); x++) {
                if ($(this).val() != null) {
                    if ($(this).val() == $("#i_pinjam_item" + x).val() && z != x) {
                        Swal.fire({
                            type: "error",
                            title: g_maaf,
                            text: g_exist,
                            confirmButtonClass: "btn btn-danger",
                        });
                        ada = true;
                        break;
                    }
                }
            }
            if (ada) {
                $(this).val("");
                $(this).html("");
            } else {
                $.ajax({
                    type: "post",
                    data: {
                        i_pinjam_item: $("#i_pinjam_item" + z).val(),
                        imahasiswa: $("#i_name").val(),
                    },
                    url: base_url + $("#path").val() + '/get_detail_pinjam',
                    dataType: "json",
                    success: function (data) {
                        $("#e_pengarang" + z).val(data.detail[0]['e_pengarang']);
                        $("#e_penerbit" + z).val(data.detail[0]['e_penerbit']);
                        $("#n_tahun_terbit" + z).val(data.detail[0]['n_tahun_terbit']);
                        $("#harga_buku" + z).val(data.detail[0]['harga_buku']);
                        $("#n_quantity" + z).val(data.detail[0]['n_quantity']);
                        $("#n_diskon" + z).val(data.detail[0]['n_diskon']);
                        $("#i_buku" + z).val(data.detail[0]['i_buku']);
                        $("#e_buku_name" + z).val(data.detail[0]['e_buku_name']);
                        hitung();
                    },
                    error: function () {
                        Swal.fire({
                            type: "error",
                            title: g_maaf,
                            text: "Error data",
                            confirmButtonClass: "btn btn-danger",
                        });
                    }
                });
            }
        });
    }

    $("#i_name").select2({
        dropdownAutoWidth: true,
        width: "100%",
        /* containerCssClass: "select-sm", */
        allowClear: true,
        ajax: {
            url: base_url + $("#path").val() + "/get_mahasiswa",
            dataType: "json",
            delay: 250,
            data: function (params) {
                var query = {
                    q: params.term,
                    i_pinjam: $("#i_pinjam").val(),
                };
                return query;
            },
            processResults: function (data) {
                return {
                    results: data,
                };
            },
            cache: false,
        },
    });

});


var Detail = $(function () {
    var i = $("#jml").val();
    $("#addrow").on("click", function () {
        i++;
        var no = $("#tabledetail tbody tr").length + 1;
        $("#jml").val(i);
        var newRow = $("<tr>");
        var cols = "";
        cols += `<td class="text-center" valign="center"><spanx id="snum${i}">${no}</spanx></td>`;
        cols += `<td><select data-nourut="${i}" required class="form-control select2-size-sm" name="i_pinjam_item[]" id="i_pinjam_item${i}"><option value=""></option></select></td>`;
        cols += `<td><input type="text" class="form-control text-right form-control-sm" id="e_pengarang${i}" name="e_pengarang[]" readonly>
        <input type="hidden" class="form-control form-control-sm" id="i_buku${i}" name="i_buku[]" readonly>
        <input type="hidden" class="form-control form-control-sm" id="e_buku_name${i}" name="e_buku_name[]" readonly>`;
        cols += `<td><input type="text" class="form-control text-right form-control-sm" id="e_penerbit${i}" name="e_penerbit[]" readonly>`;
        cols += `<td><input type="text" class="form-control text-right form-control-sm" id="n_tahun_terbit${i}" name="n_tahun_terbit[]" readonly>`;
        cols += `<td><input type="text" class="form-control text-right form-control-sm" id="harga_buku${i}" name="harga_buku[]" readonly>`;
        cols += `<td><input type="text" autocomplete="off" class="form-control text-right form-control-sm" id="n_quantity${i}" name="n_quantity[]"onkeypress="return bilanganasli(event);hitung();" onkeyup="hitung()" onblur=\"if(this.value==''){this.value='1';hitung();}\" onfocus=\"if(this.value=='1'){this.value='';}\"></td>`;
        cols += `<td><input type="text" autocomplete="off" class="form-control text-right form-control-sm" id="n_diskon${i}" name="n_diskon[]"onkeypress="return hitung();" onkeyup="hitung()"></td>`;
        cols += `<td><input type="text" class="form-control text-right form-control-sm" id="v_nilai_bersih${i}" name="v_nilai_bersih[]"> </td>`;
        cols += `<td><input type="text" class="form-control form-control-sm" id="e_remark_item" name="e_remark_item[]"></td>`;
        cols += `<td class="text-center"><i title="Delete" class="fa fa-minus-circle fa-lg text-danger ibtnDel"></i></td>`;
        newRow.append(cols);
        $("#tabledetail").append(newRow);
        $("#i_pinjam_item" + i).select2({
            placeholder: g_pilihdata,
            dropdownAutoWidth: true,
            width: '100%',
            containerCssClass: 'select-xs',
            allowClear: true,
            ajax: {
                url: base_url + $("#path").val() + "/get_pinjam",
                dataType: "json",
                delay: 250,
                data: function (params) {
                    var query = {
                        q: params.term,
                        i_name: $("#i_name").val(),
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data,
                    };
                },
                cache: false,
            },
        }).change(function (event) {
            var z = $(this).data("nourut");
            var ada = false;
            for (var x = 1; x <= $("#jml").val(); x++) {
                if ($(this).val() != null) {
                    if ($(this).val() == $("#i_pinjam_item" + x).val() && z != x) {
                        Swal.fire({
                            type: "error",
                            title: g_maaf,
                            text: g_exist,
                            confirmButtonClass: "btn btn-danger",
                        });
                        ada = true;
                        break;
                    }
                }
            }
            if (ada) {
                $(this).val("");
                $(this).html("");
            } else {
                $.ajax({
                    type: "post",
                    data: {
                        i_pinjam_item: $("#i_pinjam_item" + z).val(),
                        imahasiswa: $("#i_name").val(),
                    },
                    url: base_url + $("#path").val() + '/get_detail_pinjam',
                    dataType: "json",
                    success: function (data) {
                        $("#e_pengarang" + z).val(data.detail[0]['e_pengarang']);
                        $("#e_penerbit" + z).val(data.detail[0]['e_penerbit']);
                        $("#n_tahun_terbit" + z).val(data.detail[0]['n_tahun_terbit']);
                        $("#harga_buku" + z).val(data.detail[0]['harga_buku']);
                        $("#n_quantity" + z).val(data.detail[0]['n_quantity']);
                        $("#n_diskon" + z).val(data.detail[0]['n_diskon']);
                        $("#i_buku" + z).val(data.detail[0]['i_buku']);
                        $("#e_buku_name" + z).val(data.detail[0]['e_buku_name']);
                        hitung();
                    },
                    error: function () {
                        Swal.fire({
                            type: "error",
                            title: g_maaf,
                            text: "Error data",
                            confirmButtonClass: "btn btn-danger",
                        });
                    }
                });
            }
        });
    });



    /*----------  Hapus Baris Data Saudara  ----------*/

    $("#tabledetail").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();

        $("#jml").val(i);
        var obj = $("#tabledetail tr:visible").find("spanx");
        $.each(obj, function (key, value) {
            id = value.id;
            $("#" + id).html(key + 1);
        });

        hitung();
    });
});

function clear_tabel() {
    $("#tabledetail tbody").empty();
    $(".tfoot").val(0);
    $("#jml").val("0");
}

function hitung() {
    var jml = $("#jml").val();
    var harga_buku = 0.0;
    var sub_total = 0.0;
    for (var x = 1; x <= jml; x++) {
        if ($("#n_quantity" + x).length && $("i_pinjam" + x).val() != "") {
            harga_buku = $("#harga_buku" + x)
                .val()
                .replaceAll(",", "");
            n_quantity = $("#n_quantity" + x)
                .val()
                .replaceAll(",", "");
            n_diskon = $("#n_diskon" + x)
                .val()
                .replaceAll(",", "");

            var gross = harga_buku * n_quantity;
            var v_disc = gross * (n_diskon / 100);
            var total_baris = gross - v_disc;
            sub_total += total_baris;

            $("#v_nilai_bersih" + x).val(number_format(total_baris, 2, ".", ","));
            $("#subtotal").val(number_format(sub_total, 2, ".", ","));
        }
    }
}

function change_ndisc() {
    var sub_total = $("#subtotal").val().replaceAll(",", "");
    var foot_vdisc = $("#n_diskon").val().replaceAll(",", ".");
    var v_disc = 0;
    if (foot_vdisc > 0.0) v_disc = (sub_total * foot_vdisc) / 100;
    // $("#n_diskon").val(number_format(v_disc, 2, ".", ","));

}

$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
$('form').on('submit', function (e) { //bind event on form submit.
    let tabel = $("#tabledetail tbody tr").length;

    if (tabel < 1) {
        Swal.fire({
            type: "error",
            title: g_maaf,
            text: "Input minimum 1 item !",
            confirmButtonClass: "btn btn-danger",
        });
        return false;
    }

    e.preventDefault();
    var formData = new FormData(this);
    if (formData) {
        sweeteditv33($("#path").val(), $("#d_from").val(), $("#d_to").val(), $("#hbuku").val(), formData);
    }
});