$(document).ready(function () {
    var controller = $("#path").val();

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    $('form').on('submit', function (e) { //bind event on form submit.
        e.preventDefault();
        var formData = new FormData(this);
        var u = $('#umur').val();
        if (formData) {
            if ($("#jurusan").val() != '00') {

                if ($("#prodi").val() != '00') {

                    if ($("#jk").val() != '00') {
                        if (u >= 18) {
                            sweetaddthessa(controller, formData);
                        } else {
                            Swal.fire('Umur Harus Lebih Dari 18!');
                            return false;
                        }
                    } else {
                        Swal.fire('Pilih Jenis Kelamin!');
                        return false;
                    }
                } else {
                    Swal.fire('Pilih Prodi!');
                    return false;
                }


            } else {
                Swal.fire('Pilih Jurusan!');
                return false;
            }
        }
    });
});

function usia() {
    var u = $('#umur').val();
    if (u > 150) {
        Swal.fire('Umur Tidak Boleh Lebih Dari 150');
        $('#umur').val(150);
    }

}
